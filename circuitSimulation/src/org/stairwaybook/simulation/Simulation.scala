package org.stairwaybook.simulation

abstract class Simulation {
  // Capilalized because it's a class
  
  type Action = () => Unit
  // Takes no parameters and returns Unit
  
  case class WorkItem(time: Int, action: Action)
  // Capilalized because it's a class
  // A WorkItem has constructor parameters ``time'' when it should be done and ``action'' what should be done
  // Making it a case class implies that we automatically get accessors for the constructor parameters
  
  private var curtime = 0
  // Initialize clock to 0
  def currentTime: Int = curtime
  // defines accessor currentTime
  
  private var agenda: List[WorkItem] = List()
  // Initialize ``to do'' list with empty list
  
  // Update ``to do'' list with ``insert'' method
  // This inserts the job at the correct time in the ordered list ``agenda''
  private def insert(ag: List[WorkItem], item: WorkItem): List[WorkItem] = {
    if (ag.isEmpty || item.time < ag.head.time) item :: ag
    else ag.head :: insert(ag.tail, item)
  }
  
  // ``insert'' will be called by afterDelay which is the only way to modify the agenda
  // NB: The convention is for the method to be defined without ``='' when the return type is Unit
  def afterDelay(delay: Int)(block: => Unit) {
    val item = WorkItem(currentTime + delay, () => block)
    agenda = insert(agenda, item)
  }
  // Recall that the :=> syntax signifies that the argument is `passed by name'
  // Otherwise we would have 
  // block: () => Unit and later () => block()
  
  // The run() method requires a way to pass the the next item in the agenda
  // This method updates the agenda, updates the time by however long the action takes, 
  //and executes the action of the item
  private def next() {
    (agenda: @unchecked) match {
      case item :: rest => 
        agenda = rest
        curtime = item.time
        item.action()
    }
  }
  // The @unchecked is there to declare that we are not matching all possible cases
  // It surpresses the ``warning: match is not exhaustive'' message
  
  def run() {
    afterDelay(0) {
      println("*** simulation started; time = " + currentTime + " steps ***")
    }
    while (!agenda.isEmpty) next()
  }

}  

/************************************************************************************/
abstract class BasicCircuitSimulation extends Simulation {
  
  // Abstract methods which depend on the circuit to be simulated
  // Although they are methods, they represent constants; this is reflected in their capitalization
  def InverterDelay: Int
  def AndGateDelay: Int
  def OrGateDelay: Int
  
  // Now we get down to defining the components
  class Wire {
    
    // Initializing variables 
    private var sigVal = false
    private var actions: List[Action] = List()
    
    // Implementing the constructors
    def getSignal = sigVal
    def setSignal(s: Boolean) = 
      if (s != sigVal) {
        sigVal = s
        actions foreach (_ ())
      }
    // Sets the sigVal to ``s'' and executes all Action's in ``actions''
    
    // To add an action to the list of actions
    // This allows us to attach components to the wire
    def addAction(a: Action) = {
      actions = a :: actions
      a()
    }
    // updates list of actions and executes the action ``a''
    // The action is executed upon addition and at every subsequent update/change
    // e.g. an `inverter'' will take the input: Wire and ``addAction invertAction''
    // see below
  }
  
  /** Logic gates will be implemented as methods in BasicCircuitSimulation */ 
  
  // The inverter attaches a helper invertAction to the input: Wire 
  // invertAction gets the input signal and calls afterDelay to put {output setSigmal !inputSig}
  // into the ``agenda''.
  def inverter(input: Wire, output: Wire) = {
    def invertAction() {
      val inputSig = input.getSignal
      afterDelay(InverterDelay) {
        output setSignal !inputSig
      } 
    }
    input addAction invertAction
  }
  
  def andGate(a1: Wire, a2: Wire, output: Wire) = {
    def andAction() = {
      val a1Sig = a1.getSignal
      val a2Sig = a2.getSignal
      afterDelay(AndGateDelay) {
        output setSignal (a1Sig & a2Sig)
      }
    }
    a1 addAction andAction
    a2 addAction andAction
  }
  
  def orGate(o1: Wire, o2: Wire, output: Wire) = {
    def orAction() = {
      val o1Sig = o1.getSignal
      val o2Sig = o2.getSignal
      afterDelay(OrGateDelay) {
        output setSignal (o1Sig | o2Sig)
      }
    }
    o1 addAction orAction
    o2 addAction orAction
  }
  
  def probe(name: String, wire: Wire) {
    def probeAction() {
      println(name + " wire at time " + currentTime + " has new value " + wire.getSignal)
    }
    wire addAction probeAction
  }
}

/************************************************************************************/
abstract class CircuitSimulation extends BasicCircuitSimulation {
  
  def halfAdder(a: Wire, b: Wire, s: Wire, c: Wire) {
    val d, e = new Wire
    orGate(a, b, d)
    andGate(a, b, c)
    inverter(c, e)
    andGate(d, e, s)
  }
  
  def fullAdder(a: Wire, b: Wire, cin: Wire, sum: Wire, cout: Wire) {
    val s, c1, c2 = new Wire
    halfAdder(a, cin, s, c1)
    halfAdder(b, s, sum, c2)
    orGate(c1, c2, cout)
  }
}

