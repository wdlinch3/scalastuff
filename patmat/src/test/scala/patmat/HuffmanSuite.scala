package patmat

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import patmat.Huffman._

@RunWith(classOf[JUnitRunner])
class HuffmanSuite extends FunSuite {
  trait TestTrees {
    val t1 = Fork(Leaf('a', 2), Leaf('b', 3), List('a', 'b'), 5)
    val t2 = Fork(Fork(Leaf('a', 2), Leaf('b', 3), List('a', 'b'), 5), Leaf('d', 4), List('a', 'b', 'd'), 9)

    val charslist = List('A','B', 'A', 'A', 'G', 'H', 'B', 'B', 'C', 'A', 'D', 'E', 'A', 'A', 'F', 'A', 'A')
    val charsdata = List( ('A', 8), ('B', 3), ('G', 1), ('H', 1), ('C', 1), ('D', 1), ('E', 1), ('F', 1) )

    val bits0 = List(1)
    val bits01 = List(1,1)
    val bits1 = List(1, 0, 0, 0, 1, 0, 1, 0, 0)
    val bits2 = List(0, 1, 1, 0, 1, 0, 1, 0, 0)
    
    val leaflist0 = List(
      Leaf('A', 8),
      Leaf('B', 3),
      Leaf('C', 1),
      Leaf('D', 1),
      Leaf('E', 1),
      Leaf('F', 1),
      Leaf('G', 1),
      Leaf('H', 1))

    val leaflist0ordered = List(
      Leaf('C', 1),
      Leaf('D', 1),
      Leaf('E', 1),
      Leaf('F', 1),
      Leaf('G', 1),
      Leaf('H', 1),
      Leaf('B', 3),
      Leaf('A', 8))

    val leaflist1 = List(
      Leaf('E', 1),
      Leaf('F', 1),
      Leaf('G', 1),
      Leaf('H', 1),
      Fork(Leaf('C', 1), Leaf('D', 1), List('C', 'D'), 2),
      Leaf('B', 3),
      Leaf('A', 8))

    val leaflist2 = List(
      Leaf('G', 1),
      Leaf('H', 1),
      Fork(Leaf('C', 1), Leaf('D', 1), List('C', 'D'), 2),
      Fork(Leaf('E', 1), Leaf('F', 1), List('E', 'F'), 2),
      Leaf('B', 3),
      Leaf('A', 8))

    val leaflist3 = List(
      Fork(Leaf('C', 1), Leaf('D', 1), List('C', 'D'), 2),
      Fork(Leaf('E', 1), Leaf('F', 1), List('E', 'F'), 2),
      Fork(Leaf('G', 1), Leaf('H', 1), List('G', 'H'), 2),
      Leaf('B', 3),
      Leaf('A', 8))

    val leaflist4 = List(
      Fork(Leaf('G', 1), Leaf('H', 1), List('G', 'H'), 2),
      Leaf('B', 3),
      Fork( Fork(Leaf('C', 1), Leaf('D', 1), List('C', 'D'), 2), Fork(Leaf('E', 1), Leaf('F', 1), List('E', 'F'), 2), 
          List('C', 'D', 'E', 'F'), 4),      
      Leaf('A', 8))
  }

  test("let's test some character stuff") {
      new TestTrees {
        assert(times(charslist) === charsdata )
      }
    }
  
  test("combine leaflist stage 1") {
    new TestTrees {
      assert(combine(leaflist0ordered) === leaflist1)
    }
  }

  test("combine leaflist stage 2") {
    new TestTrees {
      assert(combine(leaflist1) === leaflist2)
    }
  }

  test("combine leaflist stage 3") {
    new TestTrees {
      assert(combine(leaflist2) === leaflist3)
    }
  }
  
  test("combine leaflist stage 4") {
    new TestTrees {
      assert(combine(leaflist3) === leaflist4)
    }
  }

  test("weight of a larger tree") {
    new TestTrees {
      assert(weight(t1) === 5)
    }
  }

  test("chars of a larger tree") {
    new TestTrees {
      assert(chars(t2) === List('a', 'b', 'd'))
    }
  }

  //  test("times list") {
  //    new TestTrees {
  //      println( "The list is "+ times(t1.chars) )
  //    }
  //  }

  test("string2chars(\"hello, world\")") {
    assert(string2Chars("hello, world") === List('h', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd'))
  }

  test("makeOrderedLeafList for some frequency table") {
    assert(makeOrderedLeafList(List(('t', 2), ('e', 1), ('x', 3))) === List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 3)))
  }

  test("combine of some leaf list") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4))
    assert(combine(leaflist) === List(Fork(Leaf('e', 1), Leaf('t', 2), List('e', 't'), 3), Leaf('x', 4)))
  }

  test("combine and reorder correctly") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 1))
    assert(combine(leaflist) === List(Leaf('x', 1), Fork(Leaf('e', 1), Leaf('t', 2), List('e', 't'), 3)))
  }

  test("combine leaf list again") {
    val newfork = Fork(Leaf('e', 1), Leaf('t', 2), List('e', 't'), 3)
    val leaflist = List(newfork, Leaf('x', 4))
    assert(combine(leaflist) === List(Fork(Fork(Leaf('e', 1), Leaf('t', 2), List('e', 't'), 3), Leaf('x', 4), List('e', 't', 'x'), 7 )) )
  }

//  test("combine t1 with leaft to get t2") {
//    val t1 = Fork(Leaf('a', 2), Leaf('b', 3), List('a', 'b'), 5)
//    val t2 = Fork(Fork(Leaf('a', 2), Leaf('b', 3), List('a', 'b'), 5), Leaf('d', 4), List('a', 'b', 'd'), 9)
//    val l1 = Leaf('d', 4)
//    val leaflist = List(l1, t1)
//    assert(combine(leaflist) === t2)
//  }
  
  test("Create CodeTree from charslist") {
    new TestTrees {
      assert ( createCodeTree(charslist) !=0)
    }
  }

  test ("Decode bits") {
    new TestTrees {
      assert( decode(t2, bits0) === List('d') )
      assert( decode(t2, bits01) === List('d','d') )
      assert( decode(t2, bits1) === List('d','a','b','b','a') )
      assert( decode(t2, bits2) === List('b', 'd','b','b','a') )
    }
  }

  test("The French Secret") {
    println(decodedSecret)
  }
  
  test("Encode a text") {
    new TestTrees {
      assert( encode(t2)("abdba".toList) === List(0,0,0,1,1,0,1,0,0) )
      assert( encode(t2)("dabba".toList) === List(1,0,0,0,1,0,1,0,0) )
    }
  }
  
  test("decode and encode a very short text; should be identity") {
    new TestTrees {
      assert(decode(t1, encode(t1)("ab".toList)) === "ab".toList)
    }
  }
  
  test("decode and encode a longer text; should be identity") {
    new TestTrees {
      assert(decode(t2, encode(t2)("ababaddbbaaddb".toList)) === "ababaddbbaaddb".toList)
    }
  }

  test("Convert code trees defined above into code tables using convert") {
    new TestTrees {
      assert (convert(t1) === List( ('a',List(0)), ('b', List(1)) )   )
      assert (convert(t2) === List( ('a',List(0,0)), ('b', List(0,1)), ('d', List(1)) )   )
    }
  }
  
  test("Encode a text using code trees converted to code tables and executed with quickEncode") {
    new TestTrees {
      assert( quickEncode(t2)("abdba".toList) === List(0,0,0,1,1,0,1,0,0) )
      assert( quickEncode(t2)("dabba".toList) === List(1,0,0,0,1,0,1,0,0) )
    }
  }
  
}
