package forcomp

import Anagrams._

object lastPart {

   //val s = List("me")
  //val s = List("man")
  val s = List("yes", "man")                      //> s  : List[java.lang.String] = List(yes, man)
  val s1 = List("I", "love", "you")               //> s1  : List[java.lang.String] = List(I, love, you)

  val sOcc: Occurrences = sentenceOccurrences(s)  //> sOcc  : forcomp.Anagrams.Occurrences = List((a,1), (e,1), (m,1), (n,1), (s,1
                                                  //| ), (y,1))
 // val sComb: List[Occurrences] = combinations(sOcc)

  // Cannot pass this directly since we would miss out on everything which is not a single word
  //  val sValid = sComb filter isWord
  // WTF is 'a'? Wow.. the dictionary is missing the first word!
  //  dictionaryByOccurrences get ('a', 1) :: Nil

  // val rest: List[Occurrences] = (for (comb <- (sComb filter isWord)) yield subtract(sOcc, comb))
  //  !(dictionaryByOccurrences get sComb.head).isEmpty

  /**
   * def candidate(list: List[Occurrences]): List[List[Product]] = list match {
   * case Nil => List(List())
   * case List(x) =>
   * for {
   * element <- list
   * if (!(dictionaryByOccurrences get element).isEmpty)
   * } yield element ::: candidate(combinations(subtract(sOcc, element)))
   * }
   * candidate(sComb)
   */
  /**
   * def sentenceAnagram0(occ: Occurrences): Occurrences =
   * (for {
   * combo <- (combinations(occ) filter isWord)
   * } yield sentenceAnagram0(subtract(occ, combo)))
   */
  /**
   * def nestMaker(occ: Occurrences): Occurrences =
   * (for {
   * combo <- (combinations(occ) filter isWord)
   * } yield nestMaker(subtract(occ, combo)))
   */

  //  sentenceAnagram0(sOcc)

  //sentenceAnagrams(s)

/**
  // This construction starts with an occurrences list occ and returns
  // another list of occurrences without the first
  def looper(occ: Occurrences): List[Occurrences] = {
    def looper0(occ0: Occurrences, accu: List[Occurrences]): List[Occurrences] = {
      if ((combinations(occ0) filter isWord).isEmpty) accu
      else looper0(subtract(occ0, (combinations(occ0) filter isWord).head), occ0 :: List())
    }
    looper0(occ, Nil)
  }
  (combinations(sOcc) filter isWord)
  (combinations(sOcc) filter isWord) map looper
*/

//  val again = for (p0 <- combinations(sOcc) filter isWord)
//   								for (p1 <- combinations(subtract(sOcc, p0)) filter isWord)
//   						      for (p2 <- combinations(subtract(subtract(sOcc, p0), p1)) filter isWord)
//   	   				yield p0
/**
  def f(occ: Occurrences): List[Sentence] = {
    def helper(occ0: Occurrences, acc: List[Sentence]): List[Sentence]  =
     if (occ0.isEmpty) acc
     else {
      for {
      elem <- (combinations(occ0) filter isWord)
    }
      yield helper(subtract(occ0, elem), (dictionaryByOccurrences get elem).toList ::: acc).flatten
    }
    helper(occ, List(Nil)) filter (x=> !x.isEmpty)
  }
 
  f(sOcc)
*/
  sentenceAnagrams(s)                             //> res0: List[forcomp.Anagrams.Sentence] = List(List(my, sane), List(my, Sean)
                                                  //| , List(yes, man), List(men, say), List(my, en, as), List(en, my, as), List(
                                                  //| say, men), List(my, as, en), List(as, my, en), List(man, yes), List(sane, m
                                                  //| y), List(Sean, my), List(en, as, my), List(as, en, my))
  sentenceAnagrams(s).length                      //> res1: Int = 14
  sentenceAnagrams(s1)                            //> res2: List[forcomp.Anagrams.Sentence] = List(List(you, olive), List(you, Io
                                                  //| , Lev), List(Io, you, Lev), List(you, Lev, Io), List(Lev, you, Io), List(ol
                                                  //| ive, you), List(Io, Lev, you), List(Lev, Io, you))
  sentenceAnagrams(s1).length                     //> res3: Int = 8
 
}