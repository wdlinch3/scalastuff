package forcomp

import Anagrams._

object power {

  /**
   * def power[T](listSet: List[T]): Set[List[T]] = {
   * if (listSet.isEmpty) Set(List())
   * else {
   * for (list <- listSet ) yield Set(List(list)) ++ power( complement(listSet, list))
   * }
   * }
   */

  val someListSet = List('a', 'b', 'c')           //> someListSet  : List[Char] = List(a, b, c)

  def complement[T](listSet: List[T], subSet: List[T]): List[T] =
    listSet filter (list => !subSet.contains(list))
                                                  //> complement: [T](listSet: List[T], subSet: List[T])List[T]

  complement(someListSet, List())                 //> res0: List[Char] = List(a, b, c)
  complement(someListSet, List('a'))              //> res1: List[Char] = List(b, c)
  complement(someListSet, someListSet)            //> res2: List[Char] = List()

  for {
    e0 <- someListSet
  } yield complement(someListSet, List(e0))       //> res3: List[List[Char]] = List(List(b, c), List(a, c), List(a, b))

  /**
   * Proof of principle for ``generator''
   *
   * def generator0(set1: List[Char], set0: List[Char]): List[List[Char]] = {
   * if (set1.isEmpty) List(set0) ::: List(List())
   * else (for (e1 <- set1) yield List(e1) :: List(e1 :: set0) ::: List(List())).flatten ::: List(set0)
   * }
   *
   *
   * val list10 = List('a')
   * val list20 = List('b')
   * val list30 = List('c')
   * generator0(generator0(list30, list20).flatten, list10)
   * ********
   */

  //  def singleton(list: List[Char]): List[List[Char]] =
  //    ( for (c <- list) yield List(c) ) ::: List(List())
  //  singleton(list4)

  val list1 = List(List(), List('a'))             //> list1  : List[List[Char]] = List(List(), List(a))
  List('a') :: List(List())                       //> res4: List[List[Char]] = List(List(a), List())
  val list2 = List(List(), List('b'))             //> list2  : List[List[Char]] = List(List(), List(b))
  val list3 = List(List(), List('c'))             //> list3  : List[List[Char]] = List(List(), List(c))

  val list4 = List('a', 'b', 'c')                 //> list4  : List[Char] = List(a, b, c)

  complement(list4, 'c' :: Nil)                   //> res5: List[Char] = List(a, b)

  /**
   * ***************************************
   * Proof of principle of ``combinations''
   * ***************************************
   */

  def combinations0(list: List[Char]): List[List[Char]] = list match {
    case Nil => List(List())
    case l :: ls => generator0(listMaker0(l), combinations0(ls))
  }                                               //> combinations0: (list: List[Char])List[List[Char]]

  def listMaker0(char: Char): List[List[Char]] =
    singleton0(char)                              //> listMaker0: (char: Char)List[List[Char]]

  def singleton0(char: Char): List[List[Char]] =
    List(char) :: List(List())                    //> singleton0: (char: Char)List[List[Char]]

  //  def listComplement[T](list: List[T], element: T): List[T] =
  //    list filter (e => e != element)

  def generator0(set1: List[List[Char]], set0: List[List[Char]]): List[List[Char]] =
    //for (e1 <- set1; e0 <- set0) yield List(e1, e0).flatten
    for (e1 <- set1; e0 <- set0) yield e1 ::: e0  //> generator0: (set1: List[List[Char]], set0: List[List[Char]])List[List[Char]
                                                  //| ]
    
// ``combinations0'' test
  combinations0(list4)                            //> res6: List[List[Char]] = List(List(a, b, c), List(a, b), List(a, c), List(a
                                                  //| ), List(b, c), List(b), List(c), List())
// Some intermediate tests
  generator0(list1, Nil)                          //> res7: List[List[Char]] = List()
  generator0(Nil, list1)                          //> res8: List[List[Char]] = List()
  generator0(list2, list1)                        //> res9: List[List[Char]] = List(List(), List(a), List(b), List(b, a))
  generator0(generator0(list3, list2), list1)     //> res10: List[List[Char]] = List(List(), List(a), List(b), List(b, a), List(c
                                                  //| ), List(c, a), List(c, b), List(c, b, a))

  /***************************************/

  val occur = ('a', 2)                            //> occur  : (Char, Int) = (a,2)
  //  listMaker(occur)
  //  listMaker(occur).flatten
  //  listMaker(occur).flatten:::List(List())

  val newVector = (for (index <- 1 to occur._2) yield (occur._1, index))
                                                  //> newVector  : scala.collection.immutable.IndexedSeq[(Char, Int)] = Vector((a
                                                  //| ,1), (a,2))

  (for (i <- 0 until newVector.length) yield newVector(i) :: Nil).toList
                                                  //> res11: List[List[(Char, Int)]] = List(List((a,1)), List((a,2)))
  (for (i <- 0 until newVector.length) yield newVector(i) :: Nil).toList ::: List(Nil)
                                                  //> res12: List[List[(Char, Int)]] = List(List((a,1)), List((a,2)), List())

  val newList = (for (index <- 1 to occur._2) yield (occur._1, index)).toList
                                                  //> newList  : List[(Char, Int)] = List((a,1), (a,2))
  for (index <- 0 until newList.length) yield newVector(index) :: List(List())
                                                  //> res13: scala.collection.immutable.IndexedSeq[List[Product]] = Vector(List((
                                                  //| a,1), List()), List((a,2), List()))

  //(List('a') foldRight List(List())) (_::_)

  (List('a') foldRight List('A'))(_ :: _)         //> res14: List[Char] = List(a, A)
  (List('a', 'b') foldRight List('A'))(_ :: _)    //> res15: List[Char] = List(a, b, A)
  /**
   * (List('a') foldLeft List('A') )(_::_)  This gives an error "value :: is not a member
   * of Char" because the elements of List('a') are but then foldLeft makes List('A') :: 'a'
   * which is not defined for cons
   */
  (List('a') foldRight List('A', 'B'))(_ :: _)    //> res16: List[Char] = List(a, A, B)
  //  (List('a') foldRight List())(_ :: _) Gives cryptic error even though
  List('a') :: List('b')                          //> res17: List[Any] = List(List(a), b)
  List('a') :: Nil                                //> res18: List[List[Char]] = List(List(a))
  // works just fine

  listMaker(occur)                                //> res19: List[forcomp.Anagrams.Occurrences] = List(List((a,1)), List((a,2)), 
                                                  //| List())
  
  val testOccurrence1 = List( ('a', 2) , ('b', 2) )
                                                  //> testOccurrence1  : List[(Char, Int)] = List((a,2), (b,2))
  //val testOccurrence2 = List( ('a', 2) , ('b', 2) )
  
  combinations(testOccurrence1)                   //> res20: List[forcomp.Anagrams.Occurrences] = List(List((a,1), (b,1)), List((
                                                  //| a,1), (b,2)), List((a,1)), List((a,2), (b,1)), List((a,2), (b,2)), List((a,
                                                  //| 2)), List((b,1)), List((b,2)), List())
  combinations(testOccurrence1).length            //> res21: Int = 9
}