package forcomp

import Anagrams._

object examples {

  val greeting = List("happy", "thanksgiving")    //> greeting  : List[java.lang.String] = List(happy, thanksgiving)

  val mixedGreeting = sentenceAnagrams(greeting).toSet
                                                  //> mixedGreeting  : scala.collection.immutable.Set[forcomp.Anagrams.Sentence] =
                                                  //|  Set(List(pig, ping, vat, sky, Hahn), List(tag, spy, van, high, pink), List(
                                                  //| hap, sky, thing, paving), List(tap, ping, van, high, sky), List(pin, pang, h
                                                  //| igh, vat, sky), List(pi, van, spy, hag, knight), List(knight, spy, van, pig,
                                                  //|  ah), List(van, hap, think, spy, gig), List(high, vat, kin, pang, spy), List
                                                  //| (vast, happy, king, nigh), List(ping, sky, pan, high, vat), List(high, spy, 
                                                  //| vat, pan, king), List(van, thing, pig, hap, sky), List(king, ping, hap, shy,
                                                  //|  vat), List(van, pith, gang, phi, sky), List(ivy, angst, Knapp, high), List(
                                                  //| night, hip, gap, sky, van), List(tap, sky, van, ping, high), List(sky, pit, 
                                                  //| pang, van, high), List(snag, ivy, Knapp, thigh), List(navy, gap, knights, hi
                                                  //| p), List(pink, ping, shy, hag, vat), List(spy, Kahn, nigh, vat, pig), List(k
                                                  //| night, van, hap, Gipsy), List(gig, Kahn, pith, spy, van), List(spy, vat, pan
                                                  //| g, kin, high), List(sky, ping, nap, vat, high), List(pig, pig, thank, van, s
                                                  //| hy), List(sky, pang, nigh, vat, phi), List(Kahn, spy, nigh, vat, pig), List(
                                                  //| tap, spy, van, king, high), List(pip, night, van, sky, hag), List(van, Skipp
                                                  //| y, tang, high), List(gap, high, pint, van, sky), List(van, sky, nigh, gap, p
                                                  //| ith), List(spy, Hahn, pink, gig, vat), List(vat, sky, pang, high, nip), List
                                                  //| (nigh, hap, king, spy, vat), List(sky, gap, pint, van, high), List(gig, than
                                                  //| k, hip, van, spy), List(vat, spy, high
                                                  //| Output exceeds cutoff limit.
  mixedGreeting.count(element => true)            //> res0: Int = 13568

  val navyGreeting = mixedGreeting filter (x => x.contains("navy"))
                                                  //> navyGreeting  : scala.collection.immutable.Set[forcomp.Anagrams.Sentence] = 
                                                  //| Set(List(navy, gap, knights, hip), List(hip, gaps, knight, navy), List(knigh
                                                  //| t, gaps, phi, navy), List(thigh, navy, gap, pinks), List(thigh, pang, navy, 
                                                  //| skip), List(skip, navy, thigh, pang), List(pinks, thigh, navy, gap), List(ga
                                                  //| p, PHIGS, navy, think), List(navy, gaps, phi, knight), List(knights, hip, ga
                                                  //| p, navy), List(ship, gap, knight, navy), List(gash, navy, pip, knight), List
                                                  //| (phi, gap, knights, navy), List(knight, gasp, navy, hip), List(navy, pinks, 
                                                  //| gap, thigh), List(hap, knight, pigs, navy), List(knight, ship, gap, navy), L
                                                  //| ist(gap, navy, knight, ship), List(gasp, navy, knight, hip), List(navy, pip,
                                                  //|  knight, gash), List(thigh, gasp, pink, navy), List(hag, navy, knights, pip)
                                                  //| , List(knights, gap, hip, navy), List(Knapp, gist, high, navy), List(pink, t
                                                  //| highs, gap, navy), List(gaps, phi, knight, navy), List(think, navy, PHIGS, g
                                                  //| ap), List(hag, knights, pip, navy), List(navy, hip, gaps, knight), List(pang
                                                  //| , skip, navy, thigh), List(gash, pip, knight, navy), List(thigh, pinks, gap,
                                                  //|  navy), List(navy, knight, pip, gash), List(knights, navy, gap, phi), List(n
                                                  //| avy, ship, knight, gap), List(pinks, navy, thigh, gap), List(gap, navy, thin
                                                  //| k, PHIGS), List(phi, navy, knight, gaps), List(pink, gasp, navy, thigh), Lis
                                                  //| t(thigh, navy, skip, pang), List(hip, knight, gaps, navy), List(gist, Knapp,
                                                  //|  high, navy), List(gasp, thigh, pink, 
                                                  //| Output exceeds cutoff limit.
  navyGreeting filter (x => x.contains("ship"))   //> res1: scala.collection.immutable.Set[forcomp.Anagrams.Sentence] = Set(List(s
                                                  //| hip, gap, knight, navy), List(knight, ship, gap, navy), List(gap, navy, knig
                                                  //| ht, ship), List(navy, ship, knight, gap), List(knight, ship, navy, gap), Lis
                                                  //| t(gap, ship, knight, navy), List(knight, navy, gap, ship), List(gap, knight,
                                                  //|  navy, ship), List(knight, gap, ship, navy), List(ship, knight, navy, gap), 
                                                  //| List(knight, gap, navy, ship), List(navy, gap, knight, ship), List(gap, knig
                                                  //| ht, ship, navy), List(navy, ship, gap, knight), List(gap, navy, ship, knight
                                                  //| ), List(navy, gap, ship, knight), List(ship, knight, gap, navy), List(gap, s
                                                  //| hip, navy, knight), List(ship, navy, gap, knight), List(ship, navy, knight, 
                                                  //| gap), List(navy, knight, gap, ship), List(navy, knight, ship, gap), List(shi
                                                  //| p, gap, navy, knight), List(knight, navy, ship, gap))

}