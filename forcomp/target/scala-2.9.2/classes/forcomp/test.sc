package forcomp

import Anagrams._

object test {
  val word0: Word = "SuperCalifragilisticExpialidocious"
                                                  //> word0  : forcomp.Anagrams.Word = SuperCalifragilisticExpialidocious
  /*
  val word1 = word0.toLowerCase.sorted.toList
  val word2 = for (letter <- word1.distinct) yield (word1 filter (l => l == letter)).length
	val word3 = word1.distinct zip word2
  val map1 =  word1.distinct -> word2
  val map2 = word3.toMap
*/
  //  wordOccurrences(word0)

  val word1 = word0.toLowerCase.sorted.toList     //> word1  : List[Char] = List(a, a, a, c, c, c, d, e, e, f, g, i, i, i, i, i, i
                                                  //| , i, l, l, l, o, o, p, p, r, r, s, s, s, t, u, u, x)
  val map1 = word1.groupBy(c => c)                //> map1  : scala.collection.immutable.Map[Char,List[Char]] = Map(e -> List(e, e
                                                  //| ), s -> List(s, s, s), x -> List(x), t -> List(t), u -> List(u, u), f -> Lis
                                                  //| t(f), a -> List(a, a, a), i -> List(i, i, i, i, i, i, i), g -> List(g), l ->
                                                  //|  List(l, l, l), p -> List(p, p), c -> List(c, c, c), r -> List(r, r), o -> L
                                                  //| ist(o, o), d -> List(d))
  (word1.distinct map map1)                       //> res0: List[List[Char]] = List(List(a, a, a), List(c, c, c), List(d), List(e,
                                                  //|  e), List(f), List(g), List(i, i, i, i, i, i, i), List(l, l, l), List(o, o),
                                                  //|  List(p, p), List(r, r), List(s, s, s), List(t), List(u, u), List(x))
  (word1.distinct map map1) map (x => x.length)   //> res1: List[Int] = List(3, 3, 1, 2, 1, 1, 7, 3, 2, 2, 2, 3, 1, 2, 1)

  // val blah = wordOccurrences("player")

  //val map2 = dictionary groupBy (w => wordOccurrences(w))

  // map2(blah)
  val occurrences = List(('a', 2), ('b', 2))      //> occurrences  : List[(Char, Int)] = List((a,2), (b,2))
  val occurrences1 = List(('a', 2), ('b', 2), ('c', 3))
                                                  //> occurrences1  : List[(Char, Int)] = List((a,2), (b,2), (c,3))

  def extendedSet(set: List[(Char, Int)]): List[(Char, Int)] = {
    for {
      (c, f) <- set
      index <- 1 to f
    } yield (c, index)
  }                                               //> extendedSet: (set: List[(Char, Int)])List[(Char, Int)]

  extendedSet(occurrences)                        //> res2: List[(Char, Int)] = List((a,1), (a,2), (b,1), (b,2))
  
	combinations(occurrences)                 //> res3: List[forcomp.Anagrams.Occurrences] = List(List((a,1), (a,2)), List((b,
                                                  //| 1), (b,2)), List())


    

}