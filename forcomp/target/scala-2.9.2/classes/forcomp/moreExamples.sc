package forcomp

import Anagrams._

object moreExamples {
 
  val happy = List("happy")                       //> happy  : List[java.lang.String] = List(happy)
  val thanksgiving = List("thanksgiving")         //> thanksgiving  : List[java.lang.String] = List(thanksgiving)
  val christmas = List("christmas")               //> christmas  : List[java.lang.String] = List(christmas)

  val rubs = List("I", "rubs", "you")             //> rubs  : List[java.lang.String] = List(I, rubs, you)
  val poops = List("poopiekjems")                 //> poops  : List[java.lang.String] = List(poopiekjems)
  
  sentenceAnagrams(poops).length                  //> res0: Int = 1782
    
  //val anagram1 = sentenceAnagrams(rubs)
  //anagram1.length

}