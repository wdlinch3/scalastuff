package forcomp

import Anagrams._

object subtract {

  val x = List(('a', 1), ('d', 1), ('l', 1), ('r', 1), ('s', 3))
                                                  //> x  : List[(Char, Int)] = List((a,1), (d,1), (l,1), (r,1), (s,3))
  val y = List(('r', 1), ('s', 1))                //> y  : List[(Char, Int)] = List((r,1), (s,1))

  val z = List(('a', 1), ('s', 2))                //> z  : List[(Char, Int)] = List((a,1), (s,2))

  def xMinusy(x: List[(Char, Int)], y: List[(Char, Int)]): List[(Char, Int)] = {
    def xMinusy0(x0: List[(Char, Int)], y0: List[(Char, Int)]): List[(Char, Int)] = y0 match {
      case Nil => x0
      case y :: yy =>
        val (xChar, xInt) = x0.unzip
        val (yChar, yInt) = y0.unzip
        val index = xChar.indexOf(yChar.head)
        val xNew: List[(Char, Int)] = xChar.zip(xInt.updated(index, xInt(index) - yInt.head))
        //x0.updated(index, (y0.head._1, x0(index)._2 - y0.head._2))
        xMinusy0(xNew, yy)
    }
    xMinusy0(x, y) flatMap dropZero
  }                                               //> xMinusy: (x: List[(Char, Int)], y: List[(Char, Int)])List[(Char, Int)]

  def dropZero(elem: (Char, Int)): List[(Char, Int)] = elem match {
    case (char, 0) => Nil
    case (char, int) => List((char, int))
  }                                               //> dropZero: (elem: (Char, Int))List[(Char, Int)]

  xMinusy(x, y)                                   //> res0: List[(Char, Int)] = List((a,1), (d,1), (l,1), (s,2))
  xMinusy(x, z)                                   //> res1: List[(Char, Int)] = List((d,1), (l,1), (r,1), (s,1))
  xMinusy(x, x)                                   //> res2: List[(Char, Int)] = List()

}