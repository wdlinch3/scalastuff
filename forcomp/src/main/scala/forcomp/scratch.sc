package forcomp

import Anagrams._

object scratch {

  val word1 = "I"                                 //> word1  : java.lang.String = I
  val word2 = "love"                              //> word2  : java.lang.String = love
  val word3 = "you"                               //> word3  : java.lang.String = you


  //val sentence1 = List(word1, word2, word3)

  val sentence1 = List("yes", "man")              //> sentence1  : List[java.lang.String] = List(yes, man)

  val sentenceToString1 = sentence1.mkString      //> sentenceToString1  : String = yesman

  val occurrence1 = sentenceOccurrences(sentence1)//> occurrence1  : forcomp.Anagrams.Occurrences = List((a,1), (e,1), (m,1), (n,1
                                                  //| ), (s,1), (y,1))
//  val combinations1 = combinations(occurrence1)
//  val number1 = combinations1.length

  //val newOccurrenceList: List[Occurrences] = for (occ <- combinations1) yield
  //combinations1(1)
  //for (occ <- combinations1) yield (occ, subtract(occurrence1, occ))

  val anag1 = List("Sean")                        //> anag1  : List[java.lang.String] = List(Sean)
  val occurrence2 = sentenceOccurrences(anag1)    //> occurrence2  : forcomp.Anagrams.Occurrences = List((a,1), (e,1), (n,1), (s,1
                                                  //| ))
  (dictionaryByOccurrences get occurrence2)       //> res0: Option[List[forcomp.Anagrams.Word]] = Some(List(sane, Sean))
  (dictionaryByOccurrences get occurrence2).toList//> res1: List[List[forcomp.Anagrams.Word]] = List(List(sane, Sean))
  
  (dictionaryByOccurrences get occurrence2).toList :: List("something")
                                                  //> res2: List[java.lang.Object] = List(List(List(sane, Sean)), something)
  (dictionaryByOccurrences get occurrence2).toList ::: List("something")
                                                  //> res3: List[java.lang.Object] = List(List(sane, Sean), something)
  (dictionaryByOccurrences get occurrence2).toList ::: List(List("something"))
                                                  //> res4: List[List[java.lang.String]] = List(List(sane, Sean), List(something))
                                                  //| 
  (for (word <- (dictionaryByOccurrences get occurrence2).toList.head)
  yield List(List("something", "somethingElse"), List("anotherThing")) map (List(word):::_)).flatten
                                                  //> res5: List[List[java.lang.String]] = List(List(sane, something, somethingEl
                                                  //| se), List(sane, anotherThing), List(Sean, something, somethingElse), List(S
                                                  //| ean, anotherThing))
  (for (word <- (dictionaryByOccurrences get occurrence2).toList.head)
  yield List(List()) map (List(word):::_)).flatten//> res6: List[List[forcomp.Anagrams.Word]] = List(List(sane), List(Sean))
  
//  for (word <- (dictionaryByOccurrences get occurrence2).toList.head)
//  yield (word :: List(List("something", "somethingElse"), List("anotherThing"))).flatten
  
  //for (occ <- combinations1) yield (dictionaryByOccurrences get occ) //::: (dictionaryByOccurrences get subtract(occurrence1, occ)) )

//  val newList0 =
//    (for (occurrence <- combinations1) yield dictionaryByOccurrences get occurrence) filter (x => !x.isEmpty)


  /**
   * val newList =
   * for (occurrence <- combinations1) yield dictionaryByOccurrences get occurrence match {
   * case None => Nil
   * case Some(List(word)) => word
   * }
   */

  //val newList2 = newList0 filter (x => !x.isEmpty)
//  val newList3 = for (term <- newList0) yield term match {
//    case Some(wordAnagrams) => wordAnagrams
//  }

//  val newList4 = newList3.flatten

//  val number2 = newList4.length

  /**
   *
   * def sentenceAnagrams0(sentence0: Sentence): List[Sentence] = {
   * val occurrenceListOfSentence: Occurrences = sentenceOccurrences(sentence1)
   *
   * val optionListWithEmpty: List[Option[List[Word]]] =
   * for (occ <- combinationsOfOccurrences) yield dictionaryByOccurrences get occ
   * val newOptionList: List[Option[List[Word]]] = optionListWithEmpty filter (x => !x.isEmpty)
   *
   * val newList: List[Word] =
   * for (term <- newList2) yield term match {
   * case Some(List(word)) => word
   * }
   *
   * }
   *
   */
   
   List("a", 1) :: Nil                            //> res7: List[List[Any]] = List(List(a, 1))
   List("a", 1) ::: Nil                           //> res8: List[Any] = List(a, 1)
   

}