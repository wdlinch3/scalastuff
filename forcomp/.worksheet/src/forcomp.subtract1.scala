package forcomp

import Anagrams._

object subtract1 {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(120); 

  val x = List(('a', 1), ('d', 1), ('l', 1), ('r', 1), ('s', 3));System.out.println("""x  : List[(Char, Int)] = """ + $show(x ));$skip(35); 
  val y = List(('r', 1), ('s', 1));System.out.println("""y  : List[(Char, Int)] = """ + $show(y ));$skip(38); 
  
  val z = List(('a', 1), ('s', 2));System.out.println("""z  : List[(Char, Int)] = """ + $show(z ));$skip(580); 

  def xMinusy(x: List[(Char, Int)], y: List[(Char, Int)]): List[(Char, Int)] = {
    def xMinusy0(x0: List[(Char, Int)], y0: List[(Char, Int)]): List[(Char, Int)] = y0 match {
      case Nil => x0
      case y :: yy =>
        val (xChar, xInt) = x0.unzip
        val (yChar, yInt) = y0.unzip
        val index = xChar.indexOf(yChar.head)
        val xNew: List[(Char, Int)] = xChar.zip(xInt.updated(index, xInt(index) - yInt.head ) )
          //x0.updated(index, (y0.head._1, x0(index)._2 - y0.head._2))
        xMinusy0(xNew, yy)
    }
    xMinusy0(x, y) flatMap dropZero
  };System.out.println("""xMinusy: (x: List[(Char, Int)], y: List[(Char, Int)])List[(Char, Int)]""");$skip(141); 

  def dropZero(elem: (Char, Int)): List[(Char, Int)] = elem match {
    case (char, 0) => Nil
    case (char, int) => List((char, int))
  };System.out.println("""dropZero: (elem: (Char, Int))List[(Char, Int)]""");$skip(21); val res$0 = 
   

  xMinusy(x, y);System.out.println("""res0: List[(Char, Int)] = """ + $show(res$0));$skip(15); val res$1 = 
  xMinusy(x,z);System.out.println("""res1: List[(Char, Int)] = """ + $show(res$1))}
  
}