package forcomp

import Anagrams._

object lastPart {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(131); 

   //val s = List("me")
  //val s = List("man")
  val s = List("yes", "man");System.out.println("""s  : List[java.lang.String] = """ + $show(s ));$skip(36); 
  val s1 = List("I", "love", "you");System.out.println("""s1  : List[java.lang.String] = """ + $show(s1 ));$skip(50); 

  val sOcc: Occurrences = sentenceOccurrences(s);System.out.println("""sOcc  : forcomp.Anagrams.Occurrences = """ + $show(sOcc ));$skip(2416); val res$0 = 
 // val sComb: List[Occurrences] = combinations(sOcc)

  // Cannot pass this directly since we would miss out on everything which is not a single word
  //  val sValid = sComb filter isWord
  // WTF is 'a'? Wow.. the dictionary is missing the first word!
  //  dictionaryByOccurrences get ('a', 1) :: Nil

  // val rest: List[Occurrences] = (for (comb <- (sComb filter isWord)) yield subtract(sOcc, comb))
  //  !(dictionaryByOccurrences get sComb.head).isEmpty

  /**
   * def candidate(list: List[Occurrences]): List[List[Product]] = list match {
   * case Nil => List(List())
   * case List(x) =>
   * for {
   * element <- list
   * if (!(dictionaryByOccurrences get element).isEmpty)
   * } yield element ::: candidate(combinations(subtract(sOcc, element)))
   * }
   * candidate(sComb)
   */
  /**
   * def sentenceAnagram0(occ: Occurrences): Occurrences =
   * (for {
   * combo <- (combinations(occ) filter isWord)
   * } yield sentenceAnagram0(subtract(occ, combo)))
   */
  /**
   * def nestMaker(occ: Occurrences): Occurrences =
   * (for {
   * combo <- (combinations(occ) filter isWord)
   * } yield nestMaker(subtract(occ, combo)))
   */

  //  sentenceAnagram0(sOcc)

  //sentenceAnagrams(s)

/**
  // This construction starts with an occurrences list occ and returns
  // another list of occurrences without the first
  def looper(occ: Occurrences): List[Occurrences] = {
    def looper0(occ0: Occurrences, accu: List[Occurrences]): List[Occurrences] = {
      if ((combinations(occ0) filter isWord).isEmpty) accu
      else looper0(subtract(occ0, (combinations(occ0) filter isWord).head), occ0 :: List())
    }
    looper0(occ, Nil)
  }
  (combinations(sOcc) filter isWord)
  (combinations(sOcc) filter isWord) map looper
*/

//  val again = for (p0 <- combinations(sOcc) filter isWord)
//   								for (p1 <- combinations(subtract(sOcc, p0)) filter isWord)
//   						      for (p2 <- combinations(subtract(subtract(sOcc, p0), p1)) filter isWord)
//   	   				yield p0
/**
  def f(occ: Occurrences): List[Sentence] = {
    def helper(occ0: Occurrences, acc: List[Sentence]): List[Sentence]  =
     if (occ0.isEmpty) acc
     else {
      for {
      elem <- (combinations(occ0) filter isWord)
    }
      yield helper(subtract(occ0, elem), (dictionaryByOccurrences get elem).toList ::: acc).flatten
    }
    helper(occ, List(Nil)) filter (x=> !x.isEmpty)
  }
 
  f(sOcc)
*/
  sentenceAnagrams(s);System.out.println("""res0: List[forcomp.Anagrams.Sentence] = """ + $show(res$0));$skip(29); val res$1 = 
  sentenceAnagrams(s).length;System.out.println("""res1: Int = """ + $show(res$1));$skip(23); val res$2 = 
  sentenceAnagrams(s1);System.out.println("""res2: List[forcomp.Anagrams.Sentence] = """ + $show(res$2));$skip(30); val res$3 = 
  sentenceAnagrams(s1).length;System.out.println("""res3: Int = """ + $show(res$3))}
 
}