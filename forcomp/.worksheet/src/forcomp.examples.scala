package forcomp

import Anagrams._

object examples {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(101); 

  val greeting = List("happy", "thanksgiving");System.out.println("""greeting  : List[java.lang.String] = """ + $show(greeting ));$skip(56); 

  val mixedGreeting = sentenceAnagrams(greeting).toSet;System.out.println("""mixedGreeting  : scala.collection.immutable.Set[forcomp.Anagrams.Sentence] = """ + $show(mixedGreeting ));$skip(39); val res$0 = 
  mixedGreeting.count(element => true);System.out.println("""res0: Int = """ + $show(res$0));$skip(69); 

  val navyGreeting = mixedGreeting filter (x => x.contains("navy"));System.out.println("""navyGreeting  : scala.collection.immutable.Set[forcomp.Anagrams.Sentence] = """ + $show(navyGreeting ));$skip(48); val res$1 = 
  navyGreeting filter (x => x.contains("ship"));System.out.println("""res1: scala.collection.immutable.Set[forcomp.Anagrams.Sentence] = """ + $show(res$1))}

}