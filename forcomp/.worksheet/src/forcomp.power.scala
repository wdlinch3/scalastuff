package forcomp

import Anagrams._

object power {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(309); 

  /**
   * def power[T](listSet: List[T]): Set[List[T]] = {
   * if (listSet.isEmpty) Set(List())
   * else {
   * for (list <- listSet ) yield Set(List(list)) ++ power( complement(listSet, list))
   * }
   * }
   */

  val someListSet = List('a', 'b', 'c');System.out.println("""someListSet  : List[Char] = """ + $show(someListSet ));$skip(119); 

  def complement[T](listSet: List[T], subSet: List[T]): List[T] =
    listSet filter (list => !subSet.contains(list));System.out.println("""complement: [T](listSet: List[T], subSet: List[T])List[T]""");$skip(35); val res$0 = 

  complement(someListSet, List());System.out.println("""res0: List[Char] = """ + $show(res$0));$skip(37); val res$1 = 
  complement(someListSet, List('a'));System.out.println("""res1: List[Char] = """ + $show(res$1));$skip(39); val res$2 = 
  complement(someListSet, someListSet);System.out.println("""res2: List[Char] = """ + $show(res$2));$skip(75); val res$3 = 

  for {
    e0 <- someListSet
  } yield complement(someListSet, List(e0));System.out.println("""res3: List[List[Char]] = """ + $show(res$3));$skip(648); 

  /**
   * Proof of principle for ``generator''
   *
   * def generator0(set1: List[Char], set0: List[Char]): List[List[Char]] = {
   * if (set1.isEmpty) List(set0) ::: List(List())
   * else (for (e1 <- set1) yield List(e1) :: List(e1 :: set0) ::: List(List())).flatten ::: List(set0)
   * }
   *
   *
   * val list10 = List('a')
   * val list20 = List('b')
   * val list30 = List('c')
   * generator0(generator0(list30, list20).flatten, list10)
   * ********
   */

  //  def singleton(list: List[Char]): List[List[Char]] =
  //    ( for (c <- list) yield List(c) ) ::: List(List())
  //  singleton(list4)

  val list1 = List(List(), List('a'));System.out.println("""list1  : List[List[Char]] = """ + $show(list1 ));$skip(28); val res$4 = 
  List('a') :: List(List());System.out.println("""res4: List[List[Char]] = """ + $show(res$4));$skip(38); 
  val list2 = List(List(), List('b'));System.out.println("""list2  : List[List[Char]] = """ + $show(list2 ));$skip(38); 
  val list3 = List(List(), List('c'));System.out.println("""list3  : List[List[Char]] = """ + $show(list3 ));$skip(35); 

  val list4 = List('a', 'b', 'c');System.out.println("""list4  : List[Char] = """ + $show(list4 ));$skip(33); val res$5 = 

  complement(list4, 'c' :: Nil);System.out.println("""res5: List[Char] = """ + $show(res$5));$skip(317); 

  /**
   * ***************************************
   * Proof of principle of ``combinations''
   * ***************************************
   */

  def combinations0(list: List[Char]): List[List[Char]] = list match {
    case Nil => List(List())
    case l :: ls => generator0(listMaker0(l), combinations0(ls))
  };System.out.println("""combinations0: (list: List[Char])List[List[Char]]""");$skip(71); 

  def listMaker0(char: Char): List[List[Char]] =
    singleton0(char);System.out.println("""listMaker0: (char: Char)List[List[Char]]""");$skip(81); 

  def singleton0(char: Char): List[List[Char]] =
    List(char) :: List(List());System.out.println("""singleton0: (char: Char)List[List[Char]]""");$skip(304); 

  //  def listComplement[T](list: List[T], element: T): List[T] =
  //    list filter (e => e != element)

  def generator0(set1: List[List[Char]], set0: List[List[Char]]): List[List[Char]] =
    //for (e1 <- set1; e0 <- set0) yield List(e1, e0).flatten
    for (e1 <- set1; e0 <- set0) yield e1 ::: e0;System.out.println("""generator0: (set1: List[List[Char]], set0: List[List[Char]])List[List[Char]]""");$skip(54); val res$6 = 
    
// ``combinations0'' test
  combinations0(list4);System.out.println("""res6: List[List[Char]] = """ + $show(res$6));$skip(52); val res$7 = 
// Some intermediate tests
  generator0(list1, Nil);System.out.println("""res7: List[List[Char]] = """ + $show(res$7));$skip(25); val res$8 = 
  generator0(Nil, list1);System.out.println("""res8: List[List[Char]] = """ + $show(res$8));$skip(27); val res$9 = 
  generator0(list2, list1);System.out.println("""res9: List[List[Char]] = """ + $show(res$9));$skip(46); val res$10 = 
  generator0(generator0(list3, list2), list1);System.out.println("""res10: List[List[Char]] = """ + $show(res$10));$skip(69); 

  /***************************************/

  val occur = ('a', 2);System.out.println("""occur  : (Char, Int) = """ + $show(occur ));$skip(174); 
  //  listMaker(occur)
  //  listMaker(occur).flatten
  //  listMaker(occur).flatten:::List(List())

  val newVector = (for (index <- 1 to occur._2) yield (occur._1, index));System.out.println("""newVector  : scala.collection.immutable.IndexedSeq[(Char, Int)] = """ + $show(newVector ));$skip(74); val res$11 = 

  (for (i <- 0 until newVector.length) yield newVector(i) :: Nil).toList;System.out.println("""res11: List[List[(Char, Int)]] = """ + $show(res$11));$skip(87); val res$12 = 
  (for (i <- 0 until newVector.length) yield newVector(i) :: Nil).toList ::: List(Nil);System.out.println("""res12: List[List[(Char, Int)]] = """ + $show(res$12));$skip(79); 

  val newList = (for (index <- 1 to occur._2) yield (occur._1, index)).toList;System.out.println("""newList  : List[(Char, Int)] = """ + $show(newList ));$skip(79); val res$13 = 
  for (index <- 0 until newList.length) yield newVector(index) :: List(List());System.out.println("""res13: scala.collection.immutable.IndexedSeq[List[Product]] = """ + $show(res$13));$skip(90); val res$14 = 

  //(List('a') foldRight List(List())) (_::_)

  (List('a') foldRight List('A'))(_ :: _);System.out.println("""res14: List[Char] = """ + $show(res$14));$skip(47); val res$15 = 
  (List('a', 'b') foldRight List('A'))(_ :: _);System.out.println("""res15: List[Char] = """ + $show(res$15));$skip(277); val res$16 = 
  /**
   * (List('a') foldLeft List('A') )(_::_)  This gives an error "value :: is not a member
   * of Char" because the elements of List('a') are but then foldLeft makes List('A') :: 'a'
   * which is not defined for cons
   */
  (List('a') foldRight List('A', 'B'))(_ :: _);System.out.println("""res16: List[Char] = """ + $show(res$16));$skip(100); val res$17 = 
  //  (List('a') foldRight List())(_ :: _) Gives cryptic error even though
  List('a') :: List('b');System.out.println("""res17: List[Any] = """ + $show(res$17));$skip(19); val res$18 = 
  List('a') :: Nil;System.out.println("""res18: List[List[Char]] = """ + $show(res$18));$skip(41); val res$19 = 
  // works just fine

  listMaker(occur);System.out.println("""res19: List[forcomp.Anagrams.Occurrences] = """ + $show(res$19));$skip(55); 
  
  val testOccurrence1 = List( ('a', 2) , ('b', 2) );System.out.println("""testOccurrence1  : List[(Char, Int)] = """ + $show(testOccurrence1 ));$skip(89); val res$20 = 
  //val testOccurrence2 = List( ('a', 2) , ('b', 2) )
  
  combinations(testOccurrence1);System.out.println("""res20: List[forcomp.Anagrams.Occurrences] = """ + $show(res$20));$skip(39); val res$21 = 
  combinations(testOccurrence1).length;System.out.println("""res21: Int = """ + $show(res$21))}
}