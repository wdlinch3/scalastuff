package forcomp

import Anagrams._

object test {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(106); 
  val word0: Word = "SuperCalifragilisticExpialidocious";System.out.println("""word0  : forcomp.Anagrams.Word = """ + $show(word0 ));$skip(323); 
  /*
  val word1 = word0.toLowerCase.sorted.toList
  val word2 = for (letter <- word1.distinct) yield (word1 filter (l => l == letter)).length
	val word3 = word1.distinct zip word2
  val map1 =  word1.distinct -> word2
  val map2 = word3.toMap
*/
  //  wordOccurrences(word0)

  val word1 = word0.toLowerCase.sorted.toList;System.out.println("""word1  : List[Char] = """ + $show(word1 ));$skip(35); 
  val map1 = word1.groupBy(c => c);System.out.println("""map1  : scala.collection.immutable.Map[Char,List[Char]] = """ + $show(map1 ));$skip(28); val res$0 = 
  (word1.distinct map map1);System.out.println("""res0: List[List[Char]] = """ + $show(res$0));$skip(48); val res$1 = 
  (word1.distinct map map1) map (x => x.length);System.out.println("""res1: List[Int] = """ + $show(res$1));$skip(166); 

  // val blah = wordOccurrences("player")

  //val map2 = dictionary groupBy (w => wordOccurrences(w))

  // map2(blah)
  val occurrences = List(('a', 2), ('b', 2));System.out.println("""occurrences  : List[(Char, Int)] = """ + $show(occurrences ));$skip(56); 
  val occurrences1 = List(('a', 2), ('b', 2), ('c', 3));System.out.println("""occurrences1  : List[(Char, Int)] = """ + $show(occurrences1 ));$skip(145); 

  def extendedSet(set: List[(Char, Int)]): List[(Char, Int)] = {
    for {
      (c, f) <- set
      index <- 1 to f
    } yield (c, index)
  };System.out.println("""extendedSet: (set: List[(Char, Int)])List[(Char, Int)]""");$skip(28); val res$2 = 

  extendedSet(occurrences);System.out.println("""res2: List[(Char, Int)] = """ + $show(res$2));$skip(30); val res$3 = 
  
	combinations(occurrences);System.out.println("""res3: List[forcomp.Anagrams.Occurrences] = """ + $show(res$3))}


    

}