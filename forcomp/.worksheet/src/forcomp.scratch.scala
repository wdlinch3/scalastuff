package forcomp

import Anagrams._

object scratch {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(71); 

  val word1 = "I";System.out.println("""word1  : java.lang.String = """ + $show(word1 ));$skip(21); 
  val word2 = "love";System.out.println("""word2  : java.lang.String = """ + $show(word2 ));$skip(20); 
  val word3 = "you";System.out.println("""word3  : java.lang.String = """ + $show(word3 ));$skip(86); 


  //val sentence1 = List(word1, word2, word3)

  val sentence1 = List("yes", "man");System.out.println("""sentence1  : List[java.lang.String] = """ + $show(sentence1 ));$skip(46); 

  val sentenceToString1 = sentence1.mkString;System.out.println("""sentenceToString1  : String = """ + $show(sentenceToString1 ));$skip(52); 

  val occurrence1 = sentenceOccurrences(sentence1);System.out.println("""occurrence1  : forcomp.Anagrams.Occurrences = """ + $show(occurrence1 ));$skip(290); 
//  val combinations1 = combinations(occurrence1)
//  val number1 = combinations1.length

  //val newOccurrenceList: List[Occurrences] = for (occ <- combinations1) yield
  //combinations1(1)
  //for (occ <- combinations1) yield (occ, subtract(occurrence1, occ))

  val anag1 = List("Sean");System.out.println("""anag1  : List[java.lang.String] = """ + $show(anag1 ));$skip(47); 
  val occurrence2 = sentenceOccurrences(anag1);System.out.println("""occurrence2  : forcomp.Anagrams.Occurrences = """ + $show(occurrence2 ));$skip(44); val res$0 = 
  (dictionaryByOccurrences get occurrence2);System.out.println("""res0: Option[List[forcomp.Anagrams.Word]] = """ + $show(res$0));$skip(51); val res$1 = 
  (dictionaryByOccurrences get occurrence2).toList;System.out.println("""res1: List[List[forcomp.Anagrams.Word]] = """ + $show(res$1));$skip(75); val res$2 = 
  
  (dictionaryByOccurrences get occurrence2).toList :: List("something");System.out.println("""res2: List[java.lang.Object] = """ + $show(res$2));$skip(73); val res$3 = 
  (dictionaryByOccurrences get occurrence2).toList ::: List("something");System.out.println("""res3: List[java.lang.Object] = """ + $show(res$3));$skip(79); val res$4 = 
  (dictionaryByOccurrences get occurrence2).toList ::: List(List("something"));System.out.println("""res4: List[List[java.lang.String]] = """ + $show(res$4));$skip(172); val res$5 = 
  (for (word <- (dictionaryByOccurrences get occurrence2).toList.head)
  yield List(List("something", "somethingElse"), List("anotherThing")) map (List(word):::_)).flatten;System.out.println("""res5: List[List[java.lang.String]] = """ + $show(res$5));$skip(122); val res$6 = 
  (for (word <- (dictionaryByOccurrences get occurrence2).toList.head)
  yield List(List()) map (List(word):::_)).flatten;System.out.println("""res6: List[List[forcomp.Anagrams.Word]] = """ + $show(res$6));$skip(1440); val res$7 = 
  
//  for (word <- (dictionaryByOccurrences get occurrence2).toList.head)
//  yield (word :: List(List("something", "somethingElse"), List("anotherThing"))).flatten
  
  //for (occ <- combinations1) yield (dictionaryByOccurrences get occ) //::: (dictionaryByOccurrences get subtract(occurrence1, occ)) )

//  val newList0 =
//    (for (occurrence <- combinations1) yield dictionaryByOccurrences get occurrence) filter (x => !x.isEmpty)


  /**
   * val newList =
   * for (occurrence <- combinations1) yield dictionaryByOccurrences get occurrence match {
   * case None => Nil
   * case Some(List(word)) => word
   * }
   */

  //val newList2 = newList0 filter (x => !x.isEmpty)
//  val newList3 = for (term <- newList0) yield term match {
//    case Some(wordAnagrams) => wordAnagrams
//  }

//  val newList4 = newList3.flatten

//  val number2 = newList4.length

  /**
   *
   * def sentenceAnagrams0(sentence0: Sentence): List[Sentence] = {
   * val occurrenceListOfSentence: Occurrences = sentenceOccurrences(sentence1)
   *
   * val optionListWithEmpty: List[Option[List[Word]]] =
   * for (occ <- combinationsOfOccurrences) yield dictionaryByOccurrences get occ
   * val newOptionList: List[Option[List[Word]]] = optionListWithEmpty filter (x => !x.isEmpty)
   *
   * val newList: List[Word] =
   * for (term <- newList2) yield term match {
   * case Some(List(word)) => word
   * }
   *
   * }
   *
   */
   
   List("a", 1) :: Nil;System.out.println("""res7: List[List[Any]] = """ + $show(res$7));$skip(24); val res$8 = 
   List("a", 1) ::: Nil;System.out.println("""res8: List[Any] = """ + $show(res$8))}
   

}