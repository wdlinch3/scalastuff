package recfun
import common._

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
    println()
    println(balance("some)(shit".toList))
    println()
    println(countChange(100, List( 1, 5, 10, 25)))
  }

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = {
    if (c == 0 || c == r) 1
    else pascal(c - 1, r - 1) + pascal(c, r - 1)
  }

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {
    def index(chars: List[Char], n: Int): Int = {
      if (chars.isEmpty || n < 0) n
      else if (chars.head == ')') index(chars.tail, n - 1)
      else if (chars.head == '(') index(chars.tail, n + 1)
      else index(chars.tail, n)
    }
    index(chars, 0) == 0
  }

  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int = {
    def change(money: Int, coins: List[Int]): Int = {
      if (money < 0 || coins.isEmpty) 0
      else if (money == 0) 1
      else change(money, coins.tail) + change(money - coins.head, coins)
    }
    
    change(money, coins)

  }

}
