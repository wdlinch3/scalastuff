package week6

object polynomials {

  //  class Poly(val terms0: Map[Int, Double]) {
  // Below, we allow entry of lists of (Int, Double) instead of Map
  class Poly(val terms0: Map[Int, Double]) {
    def this(bindings: (Int, Double)*) = this(bindings.toMap)
    //This allows to enter a variable number of data in the form (Int, Double)
    val terms = terms0 withDefaultValue 0.0
    //This returns 0.0 if queried on non-existent key (rather than throwing exception)
    def +(other: Poly) =
      new Poly((other.terms foldLeft terms)(addTerm))
    def addTerm(terms: Map[Int, Double], term: (Int, Double)): Map[Int, Double] = {
      val (exp, coef) = term
//      terms ++ Map(exp -> (coef + terms(exp)) )
      terms + (exp -> (coef + terms(exp)))
    }

    override def toString =
      (for ((exp, coef) <- terms.toList.sorted.reverse) yield coef + "x^" + exp) mkString " + "
    //the crazy shit on terms (which is a Map) is to order it
  }

  val p1 = new Poly(1 -> 2, 3 -> 4, 5 -> 6.2)     //> p1  : week6.polynomials.Poly = 6.2x^5 + 4.0x^3 + 2.0x^1
  val p2 = new Poly(0 -> 3, 3 -> 7)               //> p2  : week6.polynomials.Poly = 7.0x^3 + 3.0x^0
  p1 + p2                                         //> res0: week6.polynomials.Poly = 6.2x^5 + 11.0x^3 + 2.0x^1 + 3.0x^0
  p1.terms                                        //> res1: scala.collection.immutable.Map[Int,Double] = Map(1 -> 2.0, 3 -> 4.0, 
                                                  //| 5 -> 6.2)
  p1.terms(2)                                     //> res2: Double = 0.0
  Map(0 -> 0)                                     //> res3: scala.collection.immutable.Map[Int,Int] = Map(0 -> 0)

}




//Previous version:
/*
package week6

object polynomials {

//  class Poly(val terms0: Map[Int, Double]) {
// Below, we allow entry of lists of (Int, Double) instead of Map
  class Poly(val terms0: Map[Int, Double]) {
  	def this(bindings: (Int, Double)*) = this(bindings.toMap)
  	//This allows to enter a variable number of data in the form (Int, Double)
    val terms = terms0 withDefaultValue 0.0
    //This returns 0.0 if queried on non-existent key (rather than throwing exception)
    def +(other: Poly) = new Poly(terms ++ (other.terms map adjust))
    // ++ overwrites terms with other.terms which is fine when the two don't collide but needs to be ajusted otherwise
    def adjust(term: (Int, Double)): (Int, Double) = {
    	val (exp, coef) = term
//    	terms get exp match {
//    		case Some(coef1) => exp -> (coef + coef1)
//    		case None => exp -> coef
//    	}
//This construction is not necessary since we guarded against a possible exception
      exp -> (coef + terms(exp))

    }

    override def toString =
      (for ((exp, coef) <- terms.toList.sorted.reverse) yield coef + "x^" + exp) mkString " + "
      //the crazy shit on terms (which is a Map) is to order it
  }

//  val p1 = new Poly(Map(1 -> 2, 3 -> 4, 5 -> 6.2))
//  val p2 = new Poly(Map(0 -> 3, 3 -> 7))
//The code was modified to accept data as (1 -> 2, 3 -> 4, 5 -> 6.2) directly.
  val p1 = new Poly(1 -> 2, 3 -> 4, 5 -> 6.2)
  val p2 = new Poly(0 -> 3, 3 -> 7)
  p1 + p2
  p1.terms
  p1.terms(2)
 

}
*/