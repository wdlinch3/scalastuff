package week6

object nqueens {
  def queens(n: Int): Set[List[Int]] = {
    def placeQueens(k: Int): Set[List[Int]] =
      if (k == 0) Set(Nil)
      else
        for {
          queens <- placeQueens(k - 1)
          col <- 0 until n
          if isSafe(col, queens)
        } yield col :: queens
    def isSafe(col: Int, qs: List[Int]): Boolean = {
      val k = qs.length
    	val qsCoord = qs.reverse zip (0 until qs.length) 		//This is the list of coordinates of all previous queens
    	val qSum = qsCoord.map{ case (x, y) => x + y }			//List of values of row+col
    	val qDif = qsCoord.map{ case (x, y) => x - y }			//List of values of col-row
    	!qs.contains(col) && !qSum.contains(col+k) && !qDif.contains(col-k)
    } //The new queen is safe if:
    	//1) col != queens.col for any column in previous queens
    	//2) the rows will never agree so we don't need to worry there
    	//3) the new column cannot be on the diagonalof any previous queen
    	//The diagonals of a queen qsCoord are given in qSum and qDiff
    placeQueens(n)
  }                                               //> queens: (n: Int)Set[List[Int]]
  
  def show(queens: List[Int]) = {
    val lines =
      for (q <- queens)
      yield Vector.fill(queens.length)(" * ").updated(q, " Q ").mkString
//print
      "\n" + (lines mkString "\n")
  }                                               //> show: (queens: List[Int])java.lang.String
  
  queens(2)                                       //> res0: Set[List[Int]] = Set()
  queens(2) map show                              //> res1: scala.collection.immutable.Set[java.lang.String] = Set()
  
  queens(3)                                       //> res2: Set[List[Int]] = Set()
  queens(3) map show                              //> res3: scala.collection.immutable.Set[java.lang.String] = Set()
  
  queens(4)                                       //> res4: Set[List[Int]] = Set(List(1, 3, 0, 2), List(2, 0, 3, 1))
  queens(4) map show                              //> res5: scala.collection.immutable.Set[java.lang.String] = Set("
                                                  //|  *  Q  *  * 
                                                  //|  *  *  *  Q 
                                                  //|  Q  *  *  * 
                                                  //|  *  *  Q  * ", "
                                                  //|  *  *  Q  * 
                                                  //|  Q  *  *  * 
                                                  //|  *  *  *  Q 
                                                  //|  *  Q  *  * ")
  
  //Can clean it up a little more with
  ( queens(4) map show ) mkString "\n"            //> res6: String = "
                                                  //|  *  Q  *  * 
                                                  //|  *  *  *  Q 
                                                  //|  Q  *  *  * 
                                                  //|  *  *  Q  * 
                                                  //| 
                                                  //|  *  *  Q  * 
                                                  //|  Q  *  *  * 
                                                  //|  *  *  *  Q 
                                                  //|  *  Q  *  * "
//There are too many for n=5 but we can take only the first 3
( queens(5) take 3 map show ) mkString "\n"       //> res7: String = "
                                                  //|  Q  *  *  *  * 
                                                  //|  *  *  *  Q  * 
                                                  //|  *  Q  *  *  * 
                                                  //|  *  *  *  *  Q 
                                                  //|  *  *  Q  *  * 
                                                  //| 
                                                  //|  *  *  *  Q  * 
                                                  //|  Q  *  *  *  * 
                                                  //|  *  *  Q  *  * 
                                                  //|  *  *  *  *  Q 
                                                  //|  *  Q  *  *  * 
                                                  //| 
                                                  //|  *  *  *  *  Q 
                                                  //|  *  *  Q  *  * 
                                                  //|  Q  *  *  *  * 
                                                  //|  *  *  *  Q  * 
                                                  //|  *  Q  *  *  * "

}