package week5

import math.Ordering

object mergesort {
  def msort[T](xs: List[T])(implicit ord: Ordering[T]): List[T] = {
    val n = xs.length / 2
    if (n == 0) xs
    else {
      def merge(xs: List[T], ys: List[T]): List[T] =
        (xs, ys) match {
          case (Nil, ys) => ys
          case (xs, Nil) => xs
          case (u :: us, v :: vs) => if (ord.lt(u, v)) u :: merge(us, ys) else v :: merge(xs, vs)
        }
      val (fst, snd) = xs splitAt n
      merge(msort(fst), msort(snd))
    }
  }                                               //> msort: [T](xs: List[T])(implicit ord: scala.math.Ordering[T])List[T]
  val nums = List(2, -4, 5, 7, 1)                 //> nums  : List[Int] = List(2, -4, 5, 7, 1)
  val fruits = List("apple", "pineapple", "orange", "banana")
                                                  //> fruits  : List[java.lang.String] = List(apple, pineapple, orange, banana)

  //  msort(nums)( (x, y) => x < y )
  //  msort(fruits)( (x, y) => x.compareTo(y) < 0 )

  msort(nums)                                     //> res0: List[Int] = List(-4, 1, 2, 5, 7)
  msort(fruits)                                   //> res1: List[java.lang.String] = List(apple, banana, orange, pineapple)

  /*
object mergesort {
  def msort[T](xs: List[T])(ord: Ordering[T]): List[T] = {
    val n = xs.length / 2
    if (n == 0) xs
    else {
      def merge(xs: List[T], ys: List[T]): List[T] =
        (xs, ys) match {
          case (Nil, ys) => ys
          case (xs, Nil) => xs
          case (u :: us, v :: vs) => if ( ord.lt(u, v) ) u :: merge(us, ys) else v :: merge(xs, vs)
        }
			val (fst, snd) = xs splitAt n
			merge( msort(fst)(ord), msort(snd)(ord) )
    }
  }
  val nums = List(2, -4, 5, 7, 1)
  val fruits = List("apple", "pineapple", "orange", "banana")
  
//  msort(nums)( (x, y) => x < y )
//  msort(fruits)( (x, y) => x.compareTo(y) < 0 )

  msort(nums)(Ordering.Int)
  msort(fruits)(Ordering.String)
}

*/

  def squareList(xs: List[Int]): List[Int] = xs match {
    case Nil => xs
    case y :: ys => (y * y) :: squareList(ys)
  }                                               //> squareList: (xs: List[Int])List[Int]
  
  def squareList1(xs: List[Int]): List[Int] = xs map ( x => x*x )
                                                  //> squareList1: (xs: List[Int])List[Int]
  squareList(nums)                                //> res2: List[Int] = List(4, 16, 25, 49, 1)
  squareList1(nums)                               //> res3: List[Int] = List(4, 16, 25, 49, 1)

}