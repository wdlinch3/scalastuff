object exercise2 {

  def sum(f: Int => Int, a: Int, b: Int) = {
    def loop(a: Int, acc: Int): Int = {
      if (a > b + 1) 0
      else acc + loop(a + 1, f(a))
    }
    loop(a, 0)
  }                                               //> sum: (f: Int => Int, a: Int, b: Int)Int
  sum((x: Int) => x * x, 0, 4)                    //> res0: Int = 30

  /* the following is also possible

 def sum ( f: Int => Int, a: Int, b: Int ) = {
  	def loop( a: Int , acc: Int): Int = {
  		if ( a > b ) acc
  		else loop ( a + 1, acc + f(a) )
  		}
  	loop ( a, 0 )
  }
  
  **or better yet**
  
 def sum( f: Int => Int )( a: Int, b: Int): Int =
 	if ( a > b ) 0 else f(a) + sum(f)(a+1, b)
  

*/

  /*
	def product ( f: Int => Int ) (a: Int, b: Int): Int = {
		def loop ( a: Int, acc: Int): Int ={
			if (a > b) acc
			else loop( a + 1, acc * f(a) )
  		}
  	loop ( a, 1 )
  }
  
*/

  def product(f: Int => Int)(a: Int, b: Int): Int =
    if (a > b) 1 else f(a) * product(f)(a + 1, b) //> product: (f: Int => Int)(a: Int, b: Int)Int

  product(x => x * x)(1, 4)                       //> res1: Int = 576

	def fact(n: Int): Int = product( x => x ) ( 1, n)
                                                  //> fact: (n: Int)Int
	
	fact(5)                                   //> res2: Int = 120
	
	def general (op: (Int, Int) => Int, unit: Int )( f: Int => Int )(a: Int, b: Int): Int =
		if ( a > b ) unit
		else op( f(a) , general(op, unit)(f)( a + 1, b) )
                                                  //> general: (op: (Int, Int) => Int, unit: Int)(f: Int => Int)(a: Int, b: Int)I
                                                  //| nt
		
	general( (x,y)=>x+y , 0)(x=>x*x)(1,4)     //> res3: Int = 30
	general( (x, y)=>x*y, 1)(x=>x)(1,5)       //> res4: Int = 120
		
		
		
		
		
}