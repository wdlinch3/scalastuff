package week7

object test {
  val problem = new Pouring(Vector(4, 9))         //> problem  : week7.Pouring = week7.Pouring@a39ab89
  problem.moves                                   //> res0: scala.collection.immutable.IndexedSeq[Product with Serializable with we
                                                  //| ek7.test.problem.Move] = Vector(Empty(0), Empty(1), Fill(0), Fill(1), Pour(0,
                                                  //| 1), Pour(1,0))
  problem.pathSets.take(2).toList                 //> res1: List[Set[week7.test.problem.Path]] = List(Set(-->Vector(0, 0)), Set(Fi
                                                  //| ll(0)-->Vector(4, 0), Fill(1)-->Vector(0, 9)))
  problem.solutions(6)                            //> res2: Stream[week7.test.problem.Path] = Stream(Fill(1) Pour(1,0) Empty(0) Po
                                                  //| ur(1,0) Empty(0) Pour(1,0) Fill(1) Pour(1,0)-->Vector(4, 6), ?)
  
  val newProblem = new Pouring(Vector(4, 9, 19))  //> newProblem  : week7.Pouring = week7.Pouring@2c79a2e7
  newProblem.solutions(17)                        //> res3: Stream[week7.test.newProblem.Path] = Stream(Fill(1) Fill(0) Pour(0,2) 
                                                  //| Fill(0) Pour(1,2) Pour(0,2)-->Vector(0, 0, 17), ?)
}