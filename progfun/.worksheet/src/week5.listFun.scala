package week5

object listFun {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(65); 
  val nums = List(2, -4, 5, 7, 1);System.out.println("""nums  : List[Int] = """ + $show(nums ));$skip(62); 
  val fruits = List("apple", "pineapple", "orange", "banana");System.out.println("""fruits  : List[java.lang.String] = """ + $show(fruits ));$skip(56); 
  val repeats = List("a", "a", "a", "b", "b", "c", "a");System.out.println("""repeats  : List[java.lang.String] = """ + $show(repeats ));$skip(28); val res$0 = 

  nums filter (x => x > 0);System.out.println("""res0: List[Int] = """ + $show(res$0));$skip(30); val res$1 = 
  nums filterNot (x => x > 0);System.out.println("""res1: List[Int] = """ + $show(res$1));$skip(30); val res$2 = 
  nums partition (x => x > 0);System.out.println("""res2: (List[Int], List[Int]) = """ + $show(res$2));$skip(31); val res$3 = 

  nums takeWhile (x => x > 0);System.out.println("""res3: List[Int] = """ + $show(res$3));$skip(30); val res$4 = 
  nums dropWhile (x => x > 0);System.out.println("""res4: List[Int] = """ + $show(res$4));$skip(25); val res$5 = 
  nums span (x => x > 0);System.out.println("""res5: (List[Int], List[Int]) = """ + $show(res$5));$skip(231); 

  def pack[T](xs: List[T]): List[List[T]] = xs match {
    case Nil => Nil
    case y :: ys => //List(List(y), pack( ys.dropWhile(y => (y==ys.head)) ))
      val (first, rest) = xs span (x => x == y)
      first :: pack(rest)
  };System.out.println("""pack: [T](xs: List[T])List[List[T]]""");$skip(17); val res$6 = 

  pack(repeats);System.out.println("""res6: List[List[java.lang.String]] = """ + $show(res$6));$skip(509); 

  /*	def encode[T](xs: List[T]): List[(T, Int)] = {
		encode0[T](ys: List[List[T]]): List[(T, Int)]= ys match {
			case Nil => Nil
			case z::zs => (z.head, z.length)::encode0(zs)
			}
		encode0( pack(xs))
	}
*/

  /*  def encode[T](xs: List[T]): List[(T,Int)] = xs match {
    case Nil => Nil
    case y :: ys =>
      val (first, rest) = xs span (x => x == y)
      (first.head, first.length) :: encode(rest)
  }
*/

  def encode[T](xs: List[T]): List[(T, Int)] = pack(xs) map (ys => (ys.head, ys.length));System.out.println("""encode: [T](xs: List[T])List[(T, Int)]""");$skip(19); val res$7 = 

  encode(repeats);System.out.println("""res7: List[(java.lang.String, Int)] = """ + $show(res$7));$skip(84); 

  def concat[T](xs: List[T], ys: List[T]): List[T] =
    (xs foldRight ys)(_ :: _);System.out.println("""concat: [T](xs: List[T], ys: List[T])List[T]""");$skip(88); 

  def reverse[T](xs: List[T]): List[T] =
    (xs foldLeft List[T]())((x, y) => y :: x);System.out.println("""reverse: [T](xs: List[T])List[T]""");$skip(17); val res$8 = 

  reverse(nums);System.out.println("""res8: List[Int] = """ + $show(res$8));$skip(99); 

  def mapFun[T, U](xs: List[T], f: T => U): List[U] = (xs foldRight List[U]())( (x,y) => f(x)::y);System.out.println("""mapFun: [T, U](xs: List[T], f: T => U)List[U]""");$skip(34); val res$9 = 
	mapFun[Int,Int](nums , x=> x*x );System.out.println("""res9: List[Int] = """ + $show(res$9));$skip(74); 

  def lengthFun[T](xs: List[T]): Int = (xs foldRight 0)( (x,y) => y+1  );System.out.println("""lengthFun: [T](xs: List[T])Int""");$skip(23); val res$10 = 
  lengthFun[Int](nums);System.out.println("""res10: Int = """ + $show(res$10))}
}