import week04._

object nth {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(188); 
  def nth[T](n: Int, xs: List[T]): T = {
    if (xs.isEmpty) throw new IndexOutOfBoundsException
    else if (n == 0) xs.head
    else nth(n - 1, xs.tail)
  };System.out.println("""nth: [T](n: Int, xs: week04.List[T])T""");$skip(57); 

  val list = new Cons(1, new Cons(2, new Cons(3, Nil)));System.out.println("""list  : week04.Cons[Int] = """ + $show(list ));$skip(15); val res$0 = 
  nth(2, list);System.out.println("""res0: Int = """ + $show(res$0));$skip(122); 
//  nth(4, list)
//  nth(-1, list)

//	val zero = new Zero is WRONG: Cannot make new instances of object
	val zero = Zero;System.out.println("""zero  : week04.Zero.type = """ + $show(zero ));$skip(26); 
	val one = new Succ(Zero);System.out.println("""one  : week04.Succ = """ + $show(one ));$skip(25); 
	val two = one.successor;System.out.println("""two  : week04.Nat = """ + $show(two ));$skip(27); 
	val three = two.successor;System.out.println("""three  : week04.Nat = """ + $show(three ));$skip(15); val res$1 = 
//
	zero + one;System.out.println("""res1: week04.Nat = """ + $show(res$1));$skip(12); val res$2 = 
	one + zero;System.out.println("""res2: week04.Nat = """ + $show(res$2));$skip(15); val res$3 = 
//
	zero + two;System.out.println("""res3: week04.Nat = """ + $show(res$3));$skip(12); val res$4 = 
	two + zero;System.out.println("""res4: week04.Nat = """ + $show(res$4));$skip(11); val res$5 = 
	one + one;System.out.println("""res5: week04.Nat = """ + $show(res$5));$skip(17); val res$6 = 
//
	zero + three;System.out.println("""res6: week04.Nat = """ + $show(res$6));$skip(14); val res$7 = 
	three + zero;System.out.println("""res7: week04.Nat = """ + $show(res$7));$skip(11); val res$8 = 
	one + two;System.out.println("""res8: week04.Nat = """ + $show(res$8));$skip(11); val res$9 = 
	two + one;System.out.println("""res9: week04.Nat = """ + $show(res$9));$skip(17); val res$10 = 
	one + one + one;System.out.println("""res10: week04.Nat = """ + $show(res$10));$skip(11); val res$11 = 
	one + two;System.out.println("""res11: week04.Nat = """ + $show(res$11));$skip(27); val res$12 = 
//
  one + one + two + two;System.out.println("""res12: week04.Nat = """ + $show(res$12));$skip(19); val res$13 = 
	one + two + three;System.out.println("""res13: week04.Nat = """ + $show(res$13));$skip(14); val res$14 = 
//
	one - one;System.out.println("""res14: week04.Nat = """ + $show(res$14));$skip(11); val res$15 = 
	two - two;System.out.println("""res15: week04.Nat = """ + $show(res$15));$skip(15); val res$16 = 
	three - three;System.out.println("""res16: week04.Nat = """ + $show(res$16));$skip(14); val res$17 = 
//
	one - two;System.out.println("""res17: week04.Nat = """ + $show(res$17));$skip(13); val res$18 = 
	one - three;System.out.println("""res18: week04.Nat = """ + $show(res$18))}
}