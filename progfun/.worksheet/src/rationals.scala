object rationals {import scala.runtime.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(47); 

  val x = new Rational(1,3);System.out.println("""x  : Rational = """ + $show(x ));$skip(10); val res$0 = 
  x.numer;System.out.println("""res0: Int = """ + $show(res$0));$skip(10); val res$1 = 
  x.denom;System.out.println("""res1: Int = """ + $show(res$1));$skip(13); val res$2 = 
	//x.neg
	-x;System.out.println("""res2: Rational = """ + $show(res$2));$skip(29); 
	
	val y = new Rational(5,7);System.out.println("""y  : Rational = """ + $show(y ));$skip(21); val res$3 = 
  //x.add(y)
  x + y;System.out.println("""res3: Rational = """ + $show(res$3));$skip(28); 

	val z = new Rational(3,2);System.out.println("""z  : Rational = """ + $show(z ));$skip(59); val res$4 = 
	
	//x.sub(y).sub(z)
	//x.add(y.neg).add(z.neg)
	x - y - z;System.out.println("""res4: Rational = """ + $show(res$4));$skip(19); val res$5 = 
	//y.add(y)
	y + y;System.out.println("""res5: Rational = """ + $show(res$5));$skip(7); val res$6 = 
	x < y;System.out.println("""res6: Boolean = """ + $show(res$6));$skip(9); val res$7 = 
	x max y;System.out.println("""res7: Rational = """ + $show(res$7))}
	
}

class Rational( x: Int, y: Int) {
	require( y != 0, "Denominator must be non-zero.")

	private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b )
	
	def numer = x
	def denom = y
	
	//def less(that: Rational): Boolean = numer * that.denom < that.numer * denom 			a better way is
	def < (that: Rational): Boolean = numer * that.denom < that.numer * denom
	
	def max(that: Rational): Rational = if (this < that) that else this
	
	// def add(that: Rational) =		a better way is
	def + (that: Rational) =
		new Rational(
			numer * that.denom + that.numer * denom,
			denom * that.denom )
	
	override def toString = {
		val g = gcd(numer, denom)
		numer/g + "/" + denom/g
	}
	
	//def neg() = new Rational(-numer, denom ) 			// also just neg: Ratoinal =
	def unary_- :Rational = new Rational(-numer, denom )
			
	//def sub(that: Rational) = add(that.neg) 	or better
	//def - (that: Rational) = this + that.neg
	def - (that: Rational) = this + -that
	
}