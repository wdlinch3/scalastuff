package week6

object maps {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(87); 
  
  val romanNumerals = Map("I" -> 1, "V" -> 5, "X"-> 10);System.out.println("""romanNumerals  : scala.collection.immutable.Map[java.lang.String,Int] = """ + $show(romanNumerals ));$skip(77); 
  val capitalOfCountry = Map("US" -> "Washington" , "Switzerland" -> "Bern");System.out.println("""capitalOfCountry  : scala.collection.immutable.Map[java.lang.String,java.lang.String] = """ + $show(capitalOfCountry ));$skip(97); val res$0 = 
  
//	capitalOfCountry("US")
//	capitalOfCountry("Andorra")
//Better:
	capitalOfCountry get "US";System.out.println("""res0: Option[java.lang.String] = """ + $show(res$0));$skip(32); val res$1 = 
	capitalOfCountry get "Andorra";System.out.println("""res1: Option[java.lang.String] = """ + $show(res$1))}
}