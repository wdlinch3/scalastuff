package week7

object primes {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(80); 
  def from(n: Int): Stream[Int] = n #:: from(n+1);System.out.println("""from: (n: Int)Stream[Int]""");$skip(23); 
  
  val nat = from(0);System.out.println("""nat  : Stream[Int] = """ + $show(nat ));$skip(18); 
  val N = from(1);System.out.println("""N  : Stream[Int] = """ + $show(N ));$skip(28); 
  val m4s = nat map (_ * 4);System.out.println("""m4s  : scala.collection.immutable.Stream[Int] = """ + $show(m4s ));$skip(26); val res$0 = 
  
  (m4s take 10).toList;System.out.println("""res0: List[Int] = """ + $show(res$0));$skip(106); 
  
 	def sieve(s: Stream[Int]): Stream[Int] = {
 	  s.head #:: sieve(s.tail filter (_ % s.head !=0) )
 	};System.out.println("""sieve: (s: Stream[Int])Stream[Int]""");$skip(31); 
 
 val primes = sieve(from(2));System.out.println("""primes  : Stream[Int] = """ + $show(primes ));$skip(27); val res$1 = 
 
 (primes take 31).toList;System.out.println("""res1: List[Int] = """ + $show(res$1));$skip(208); 
  /* Back to square roots */
  def sqrtStream(x: Double): Stream[Double] = {
  	def improve(guess: Double) = (guess + x/guess)/2
  	lazy val guesses: Stream[Double] = 1 #:: guesses map improve
  	guesses
  };System.out.println("""sqrtStream: (x: Double)Stream[Double]""");$skip(36); val res$2 = 
  
  (sqrtStream(4) take 10).toList;System.out.println("""res2: List[Double] = """ + $show(res$2));$skip(110); 
  
  def isGoodEnough(guess: Double, x: Double): Boolean = {
    math.abs( (guess*guess - x)/x ) < 0.0001
  };System.out.println("""isGoodEnough: (guess: Double, x: Double)Boolean""");$skip(46); val res$3 = 
  
  sqrtStream(4).filter(isGoodEnough(_, 4));System.out.println("""res3: scala.collection.immutable.Stream[Double] = """ + $show(res$3))}
  
}