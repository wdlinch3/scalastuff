object scratch {

  //Base trait
  trait List[T] {
    def isEmpty: Boolean
    def head: T
    def tail: List[T]

  }
  //Implementations
  class Cons[T](val head: T, val tail: List[T]) extends List[T] {
    def isEmpty = false
    //Note that head and tail are already implemented
  }
  class Nil[T] extends List[T] {
    def isEmpty: Boolean = true
    def head: Nothing = throw new NoSuchElementException("Nil.head")
    def tail: Nothing = throw new NoSuchElementException("Nil.tail")
  }import scala.runtime.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(769); 

  def nth(n: Int, list: List[Any]): Any = {
    def nth0(n: Int, list: List[Any], i: Int): List[Any] = {
      if (i < n) nth0(n, list.tail, i + 1)
      else if (i == n) list
      else throw new IndexOutOfBoundsException
    }
    nth0(n: Int, list: List[Any], 0).head
  };System.out.println("""nth: (n: Int, list: scratch.List[Any])Any""")}
}