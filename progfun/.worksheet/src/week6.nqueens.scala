package week6

object nqueens {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(1053); 
  def queens(n: Int): Set[List[Int]] = {
    def placeQueens(k: Int): Set[List[Int]] =
      if (k == 0) Set(Nil)
      else
        for {
          queens <- placeQueens(k - 1)
          col <- 0 until n
          if isSafe(col, queens)
        } yield col :: queens
    def isSafe(col: Int, qs: List[Int]): Boolean = {
      val k = qs.length
    	val qsCoord = qs.reverse zip (0 until qs.length) 		//This is the list of coordinates of all previous queens
    	val qSum = qsCoord.map{ case (x, y) => x + y }			//List of values of row+col
    	val qDif = qsCoord.map{ case (x, y) => x - y }			//List of values of col-row
    	!qs.contains(col) && !qSum.contains(col+k) && !qDif.contains(col-k)
    } //The new queen is safe if:
    	//1) col != queens.col for any column in previous queens
    	//2) the rows will never agree so we don't need to worry there
    	//3) the new column cannot be on the diagonalof any previous queen
    	//The diagonals of a queen qsCoord are given in qSum and qDiff
    placeQueens(n)
  };System.out.println("""queens: (n: Int)Set[List[Int]]""");$skip(197); 
  
  def show(queens: List[Int]) = {
    val lines =
      for (q <- queens)
      yield Vector.fill(queens.length)(" * ").updated(q, " Q ").mkString
//print
      "\n" + (lines mkString "\n")
  };System.out.println("""show: (queens: List[Int])java.lang.String""");$skip(15); val res$0 = 
  
  queens(2);System.out.println("""res0: Set[List[Int]] = """ + $show(res$0));$skip(21); val res$1 = 
  queens(2) map show;System.out.println("""res1: scala.collection.immutable.Set[java.lang.String] = """ + $show(res$1));$skip(15); val res$2 = 
  
  queens(3);System.out.println("""res2: Set[List[Int]] = """ + $show(res$2));$skip(21); val res$3 = 
  queens(3) map show;System.out.println("""res3: scala.collection.immutable.Set[java.lang.String] = """ + $show(res$3));$skip(15); val res$4 = 
  
  queens(4);System.out.println("""res4: Set[List[Int]] = """ + $show(res$4));$skip(21); val res$5 = 
  queens(4) map show;System.out.println("""res5: scala.collection.immutable.Set[java.lang.String] = """ + $show(res$5));$skip(81); val res$6 = 
  
  //Can clean it up a little more with
  ( queens(4) map show ) mkString "\n";System.out.println("""res6: String = """ + $show(res$6));$skip(106); val res$7 = 
//There are too many for n=5 but we can take only the first 3
( queens(5) take 3 map show ) mkString "\n";System.out.println("""res7: String = """ + $show(res$7))}

}