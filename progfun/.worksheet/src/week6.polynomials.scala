package week6

object polynomials {

  //  class Poly(val terms0: Map[Int, Double]) {
  // Below, we allow entry of lists of (Int, Double) instead of Map
  class Poly(val terms0: Map[Int, Double]) {
    def this(bindings: (Int, Double)*) = this(bindings.toMap)
    //This allows to enter a variable number of data in the form (Int, Double)
    val terms = terms0 withDefaultValue 0.0
    //This returns 0.0 if queried on non-existent key (rather than throwing exception)
    def +(other: Poly) =
      new Poly((other.terms foldLeft terms)(addTerm))
    def addTerm(terms: Map[Int, Double], term: (Int, Double)): Map[Int, Double] = {
      val (exp, coef) = term
//      terms ++ Map(exp -> (coef + terms(exp)) )
      terms + (exp -> (coef + terms(exp)))
    }

    override def toString =
      (for ((exp, coef) <- terms.toList.sorted.reverse) yield coef + "x^" + exp) mkString " + "
    //the crazy shit on terms (which is a Map) is to order it
  };import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(999); 

  val p1 = new Poly(1 -> 2, 3 -> 4, 5 -> 6.2);System.out.println("""p1  : week6.polynomials.Poly = """ + $show(p1 ));$skip(36); 
  val p2 = new Poly(0 -> 3, 3 -> 7);System.out.println("""p2  : week6.polynomials.Poly = """ + $show(p2 ));$skip(10); val res$0 = 
  p1 + p2;System.out.println("""res0: week6.polynomials.Poly = """ + $show(res$0));$skip(11); val res$1 = 
  p1.terms;System.out.println("""res1: scala.collection.immutable.Map[Int,Double] = """ + $show(res$1));$skip(14); val res$2 = 
  p1.terms(2);System.out.println("""res2: Double = """ + $show(res$2));$skip(14); val res$3 = 
  Map(0 -> 0);System.out.println("""res3: scala.collection.immutable.Map[Int,Int] = """ + $show(res$3))}

}




//Previous version:
/*
package week6

object polynomials {

//  class Poly(val terms0: Map[Int, Double]) {
// Below, we allow entry of lists of (Int, Double) instead of Map
  class Poly(val terms0: Map[Int, Double]) {
  	def this(bindings: (Int, Double)*) = this(bindings.toMap)
  	//This allows to enter a variable number of data in the form (Int, Double)
    val terms = terms0 withDefaultValue 0.0
    //This returns 0.0 if queried on non-existent key (rather than throwing exception)
    def +(other: Poly) = new Poly(terms ++ (other.terms map adjust))
    // ++ overwrites terms with other.terms which is fine when the two don't collide but needs to be ajusted otherwise
    def adjust(term: (Int, Double)): (Int, Double) = {
    	val (exp, coef) = term
//    	terms get exp match {
//    		case Some(coef1) => exp -> (coef + coef1)
//    		case None => exp -> coef
//    	}
//This construction is not necessary since we guarded against a possible exception
      exp -> (coef + terms(exp))

    }

    override def toString =
      (for ((exp, coef) <- terms.toList.sorted.reverse) yield coef + "x^" + exp) mkString " + "
      //the crazy shit on terms (which is a Map) is to order it
  }

//  val p1 = new Poly(Map(1 -> 2, 3 -> 4, 5 -> 6.2))
//  val p2 = new Poly(Map(0 -> 3, 3 -> 7))
//The code was modified to accept data as (1 -> 2, 3 -> 4, 5 -> 6.2) directly.
  val p1 = new Poly(1 -> 2, 3 -> 4, 5 -> 6.2)
  val p2 = new Poly(0 -> 3, 3 -> 7)
  p1 + p2
  p1.terms
  p1.terms(2)
 

}
*/