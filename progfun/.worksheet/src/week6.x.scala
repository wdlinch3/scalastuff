package week6

import scala.io.Source

object x {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(162); 

  val in = Source.fromURL("http://lamp.epfl.ch/files/content/sites/lamp/files/teaching/progfun/linuxwords.txt");System.out.println("""in  : scala.io.BufferedSource = """ + $show(in ));$skip(28); 

  val words0 = in.getLines;System.out.println("""words0  : Iterator[String] = """ + $show(words0 ));$skip(82); 
	
	val words = words0.toList filter (word => word forall (char => char.isLetter));System.out.println("""words  : List[String] = """ + $show(words ));$skip(141); 

  val mnem = Map(
    '2' -> "ABC", '3' -> "DEF", '4' -> "GHI", '5' -> "JKL",
    '6' -> "MNO", '7' -> "PQRS", '8' -> "TUV", '9' -> "WXYZ");System.out.println("""mnem  : scala.collection.immutable.Map[Char,java.lang.String] = """ + $show(mnem ));$skip(128); 
  
  /** Invert the mnem map */
  val charCode: Map[Char, Char] =
    for ((digit, str) <- mnem; ltr <- str) yield ltr -> digit;System.out.println("""charCode  : Map[Char,Char] = """ + $show(charCode ));$skip(118); 
  
  /** Maps a word to it's digit string */
  def wordCode(word: String): String =
    word.toUpperCase map charCode;System.out.println("""wordCode: (word: String)String""");$skip(220); 
  
  /** e.g. wordCode("java")  returns 5282 */
  
  /** Need a map from integer strings to all words that represent them*/
  val wordsForNum: Map[String, Seq[String]] =
    words groupBy wordCode withDefaultValue Seq();System.out.println("""wordsForNum  : Map[String,Seq[String]] = """ + $show(wordsForNum ));$skip(394); 

  def encode(number: String): Set[List[String]] =
//    number map wordsForNum
// This does not work because such a construction needs to run through chars not strings
    if (number.isEmpty) Set(List())
    else {
      for {
        split <- 1 to number.length
        word <- wordsForNum(number take split)
        rest <- encode(number drop split)
      } yield word :: rest
      }.toSet;System.out.println("""encode: (number: String)Set[List[String]]""");$skip(121); val res$0 = 
      //We have to convert the whole shebang to Set from IndexedSeq which is happening because of split


encode("5282");System.out.println("""res0: Set[List[String]] = """ + $show(res$0));$skip(15); val res$1 = 
encode("2608");System.out.println("""res1: Set[List[String]] = """ + $show(res$1));$skip(15); val res$2 = 
encode("2614");System.out.println("""res2: Set[List[String]] = """ + $show(res$2));$skip(21); val res$3 = 
encode("4438462608");System.out.println("""res3: Set[List[String]] = """ + $show(res$3));$skip(21); val res$4 = 
encode("4438462614");System.out.println("""res4: Set[List[String]] = """ + $show(res$4));$skip(21); val res$5 = 
encode("7225247386");System.out.println("""res5: Set[List[String]] = """ + $show(res$5));$skip(86); 
  def translate(number: String): Set[String] =
  	encode(number) map (_ mkString " ");System.out.println("""translate: (number: String)Set[String]""");$skip(25); val res$6 = 

translate("7225247386");System.out.println("""res6: Set[String] = """ + $show(res$6))}

}