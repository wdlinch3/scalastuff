object intsets {import scala.runtime.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(372); 
/*  val t1 = new NonEmpty(6,new Empty, new Empty)
  val t2 = t1 incl 4
  val t3 = t2 incl 3
  val t4 = t3 incl 5
  val t = t4 incl 7 incl 9 incl 8
  
  val s1 = new NonEmpty(10 , new Empty, new Empty)
  val s2 = s1 incl 9 incl 7 incl 1 incl 8
  val s = s2 incl 12 incl 11 incl 14
  
  val u = t.union(s)
*/
  val t1 = new NonEmpty(2, new Empty, new Empty);System.out.println("""t1  : NonEmpty = """ + $show(t1 ));$skip(27); 
  val t = t1 incl 1 incl 3;System.out.println("""t  : IntSet = """ + $show(t ));$skip(49); 
  val s1 = new NonEmpty(4, new Empty, new Empty);System.out.println("""s1  : NonEmpty = """ + $show(s1 ));$skip(27); 
  val s = s1 incl 3 incl 5;System.out.println("""s  : IntSet = """ + $show(s ));$skip(20); 
  val u = t union s;System.out.println("""u  : IntSet = """ + $show(u ));$skip(20); 
  val v = s union t;System.out.println("""v  : IntSet = """ + $show(v ))}
}

abstract class IntSet {
  def incl(x: Int): IntSet
  def contains(x: Int): Boolean
  def union(other: IntSet): IntSet
}

class Empty extends IntSet {
	def incl(x: Int): IntSet = new NonEmpty(x, new Empty, new Empty)
	def contains(x: Int): Boolean = false
	override def toString = "."
	def union(other: IntSet): IntSet = other
}

class NonEmpty(elem: Int, left: IntSet, right: IntSet) extends IntSet {
	def incl(x: Int): IntSet = {
		if (x < elem) new NonEmpty(elem, left incl x, right) 					//Note: Making entirely new tree (ie not overwriting; no mutation)
		else if (x > elem) new NonEmpty(elem, left, right incl x)			//aka "persistent data structure"
		else this
	}
	def contains(x: Int): Boolean = {
		if (x < elem)	left contains x
		else if (x > elem) right contains x
		else true
	}
	override def toString = "{" + left + elem + right + "}"
	def union(other: IntSet): IntSet = ((left union right) union other) incl elem
}