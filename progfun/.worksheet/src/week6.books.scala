package week6

object books {
  case class Book(title: String, authors: List[String]);import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(772); 

  val books = Set( 		//List(  this was previously defined as a list but a duplication problem suggested this change
    Book(
      title = "Structure and Interpretation of Computer Programs",
      authors = List("Abelson, Harald", "Sussman, Gerald J.")),
    Book(
      title = "Introduction to Functional Programming",
      authors = List("Bird, Richard", "Wadler, Phil")),
    Book(
      title = "Effective Java",
      authors = List("Bloch, Joshua")),
    Book(
      title = "Java Puzzlers",
      authors = List("Bloch, Joshua", "Gafter, Neal")),
    Book(
      title = "Programming in Scala",
      authors = List("Odersky, Martin", "Spoon, Lex", "Venners, Bill"))
      );System.out.println("""books  : scala.collection.immutable.Set[week6.books.Book] = """ + $show(books ));$skip(78); val res$0 = 

for {
	b <- books
	a <- b.authors
	if a startsWith "Bloch,"
	} yield b.title;System.out.println("""res0: scala.collection.immutable.Set[String] = """ + $show(res$0));$skip(230); val res$1 = 

for {
	b1 <- books
	b2 <- books
	if (b2 != b1) //then the books appear as (book1, book2) and (book2, book1) but in Set these are the same
	//if (b1.title < b2.title)
	a1 <- b1.authors
	a2 <- b2.authors
	if (a1 == a2)
	} yield a1;System.out.println("""res1: scala.collection.immutable.Set[String] = """ + $show(res$1));$skip(600); val res$2 = 
	
//The problem of duplication is avoided by changing List to Set as the latter has no order and no duplication

//Task: Translate
// for (b <- books; a <- b.authors; if a startsWith "Bloch,")
// yield b.title
//into higher-order functions
//First rewrite
//books flatMap (b =>
//	for (a <- b.authors; if a startsWith "Bloch,")
//	yield b.title )
//Then change if
//  books flatMap (b =>
//	  for (a <- b.authors withFilter(a => a.startsWith "Bloch,")) yield b.title
//And finally change the last for loop
	  books flatMap (b => b.authors withFilter (a => a startsWith "Bloch,") map (a => b.title) );System.out.println("""res2: scala.collection.immutable.Set[String] = """ + $show(res$2))}
	  
	   

//booksfilter (b => b.authors startsWith "Bloch,").map(b => b.title)



}