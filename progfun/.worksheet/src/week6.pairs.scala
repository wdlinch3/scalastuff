package week6

object pairs {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(42); 

  val n = 7;System.out.println("""n  : Int = """ + $show(n ));$skip(72); 

  val xss = (1 until n) map (i =>
    ((1 until i) map (j => (i, j))));System.out.println("""xss  : scala.collection.immutable.IndexedSeq[scala.collection.immutable.IndexedSeq[(Int, Int)]] = """ + $show(xss ));$skip(146); val res$0 = 
  //This command flattens the list
  //(xss foldRight Seq[(Int, Int)]() ) (_ ++ _)
  //But there is the following shorthand for it:
  xss.flatten;System.out.println("""res0: scala.collection.immutable.IndexedSeq[(Int, Int)] = """ + $show(res$0));$skip(170); val res$1 = 
  //The shorter version of all of this uses
  //(xs flatMap f) = (xs map f).flatten
  //So, we can write
  (1 until n) flatMap (i =>
    ((1 until i) map (j => (i, j))));System.out.println("""res1: scala.collection.immutable.IndexedSeq[(Int, Int)] = """ + $show(res$1));$skip(166); 
  //To filter on whether the sum of the pairs is prime we use the previous definition of isPrime
  def isPrime(n: Int): Boolean = (2 until n).forall(c => n % c != 0);System.out.println("""isPrime: (n: Int)Boolean""");$skip(138); val res$2 = 
  //and filter the result above
  (1 until n) flatMap (i =>
    ((1 until i) map (j => (i, j)))) filter { case (x, y) => isPrime(x + y) };System.out.println("""res2: scala.collection.immutable.IndexedSeq[(Int, Int)] = """ + $show(res$2));$skip(125); val res$3 = 
  //Same implementation with for syntax
  for {
    i <- 1 until n
    j <- i until i
    if isPrime(i + j)
  } yield (i, j);System.out.println("""res3: scala.collection.immutable.IndexedSeq[(Int, Int)] = """ + $show(res$3));$skip(126); 

  def dumbScalarProduct(v1: List[Double], v2: List[Double]): Double = (for (i <- 0 until v1.length) yield v1(i) * v2(i)).sum;System.out.println("""dumbScalarProduct: (v1: List[Double], v2: List[Double])Double""");$skip(159); 
  
  //A cleverer implementation uses zip:
  def scalarProduct(v1: List[Double], v2: List[Double]): Double = ( for ( (x, y) <- v1 zip v2 ) yield (x * y) ).sum;System.out.println("""scalarProduct: (v1: List[Double], v2: List[Double])Double""")}

}