object exercise2 {import scala.runtime.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(186); 
  def sum(f: Int => Int, a: Int, b: Int) = {
    def loop(a: Int, acc: Int): Int = {
      if (a > b + 1) 0
      else acc + loop(a + 1, f(a))
    }
    loop(a, 0)
  };System.out.println("""sum: (f: Int => Int, a: Int, b: Int)Int""");$skip(31); val res$0 = 
  sum((x: Int) => x * x, 0, 4);System.out.println("""res0: Int = """ + $show(res$0));$skip(627); 

  /* the following is also possible

 def sum ( f: Int => Int, a: Int, b: Int ) = {
  	def loop( a: Int , acc: Int): Int = {
  		if ( a > b ) acc
  		else loop ( a + 1, acc + f(a) )
  		}
  	loop ( a, 0 )
  }
  
  **or better yet**
  
 def sum( f: Int => Int )( a: Int, b: Int): Int =
 	if ( a > b ) 0 else f(a) + sum(f)(a+1, b)
  

*/

  /*
	def product ( f: Int => Int ) (a: Int, b: Int): Int = {
		def loop ( a: Int, acc: Int): Int ={
			if (a > b) acc
			else loop( a + 1, acc * f(a) )
  		}
  	loop ( a, 1 )
  }
  
*/

  def product(f: Int => Int)(a: Int, b: Int): Int =
    if (a > b) 1 else f(a) * product(f)(a + 1, b);System.out.println("""product: (f: Int => Int)(a: Int, b: Int)Int""");$skip(29); val res$1 = 

  product(x => x * x)(1, 4);System.out.println("""res1: Int = """ + $show(res$1));$skip(52); 

	def fact(n: Int): Int = product( x => x ) ( 1, n);System.out.println("""fact: (n: Int)Int""");$skip(11); val res$2 = 
	
	fact(5);System.out.println("""res2: Int = """ + $show(res$2));$skip(163); 
	
	def general (op: (Int, Int) => Int, unit: Int )( f: Int => Int )(a: Int, b: Int): Int =
		if ( a > b ) unit
		else op( f(a) , general(op, unit)(f)( a + 1, b) );System.out.println("""general: (op: (Int, Int) => Int, unit: Int)(f: Int => Int)(a: Int, b: Int)Int""");$skip(42); val res$3 = 
		
	general( (x,y)=>x+y , 0)(x=>x*x)(1,4);System.out.println("""res3: Int = """ + $show(res$3));$skip(37); val res$4 = 
	general( (x, y)=>x*y, 1)(x=>x)(1,5);System.out.println("""res4: Int = """ + $show(res$4))}
		
		
		
		
		
}