package week5

import math.Ordering

object mergesort {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(509); 
  def msort[T](xs: List[T])(implicit ord: Ordering[T]): List[T] = {
    val n = xs.length / 2
    if (n == 0) xs
    else {
      def merge(xs: List[T], ys: List[T]): List[T] =
        (xs, ys) match {
          case (Nil, ys) => ys
          case (xs, Nil) => xs
          case (u :: us, v :: vs) => if (ord.lt(u, v)) u :: merge(us, ys) else v :: merge(xs, vs)
        }
      val (fst, snd) = xs splitAt n
      merge(msort(fst), msort(snd))
    }
  };System.out.println("""msort: [T](xs: List[T])(implicit ord: scala.math.Ordering[T])List[T]""");$skip(34); 
  val nums = List(2, -4, 5, 7, 1);System.out.println("""nums  : List[Int] = """ + $show(nums ));$skip(62); 
  val fruits = List("apple", "pineapple", "orange", "banana");System.out.println("""fruits  : List[java.lang.String] = """ + $show(fruits ));$skip(105); val res$0 = 

  //  msort(nums)( (x, y) => x < y )
  //  msort(fruits)( (x, y) => x.compareTo(y) < 0 )

  msort(nums);System.out.println("""res0: List[Int] = """ + $show(res$0));$skip(16); val res$1 = 
  msort(fruits);System.out.println("""res1: List[java.lang.String] = """ + $show(res$1));$skip(856); 

  /*
object mergesort {
  def msort[T](xs: List[T])(ord: Ordering[T]): List[T] = {
    val n = xs.length / 2
    if (n == 0) xs
    else {
      def merge(xs: List[T], ys: List[T]): List[T] =
        (xs, ys) match {
          case (Nil, ys) => ys
          case (xs, Nil) => xs
          case (u :: us, v :: vs) => if ( ord.lt(u, v) ) u :: merge(us, ys) else v :: merge(xs, vs)
        }
			val (fst, snd) = xs splitAt n
			merge( msort(fst)(ord), msort(snd)(ord) )
    }
  }
  val nums = List(2, -4, 5, 7, 1)
  val fruits = List("apple", "pineapple", "orange", "banana")
  
//  msort(nums)( (x, y) => x < y )
//  msort(fruits)( (x, y) => x.compareTo(y) < 0 )

  msort(nums)(Ordering.Int)
  msort(fruits)(Ordering.String)
}

*/

  def squareList(xs: List[Int]): List[Int] = xs match {
    case Nil => xs
    case y :: ys => (y * y) :: squareList(ys)
  };System.out.println("""squareList: (xs: List[Int])List[Int]""");$skip(69); 
  
  def squareList1(xs: List[Int]): List[Int] = xs map ( x => x*x );System.out.println("""squareList1: (xs: List[Int])List[Int]""");$skip(19); val res$2 = 
  squareList(nums);System.out.println("""res2: List[Int] = """ + $show(res$2));$skip(20); val res$3 = 
  squareList1(nums);System.out.println("""res3: List[Int] = """ + $show(res$3))}

}