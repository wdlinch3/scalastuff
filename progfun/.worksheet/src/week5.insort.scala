package week5

object insort {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(79); 
  val fruit = List("apples", "oranges", "pears");System.out.println("""fruit  : List[java.lang.String] = """ + $show(fruit ));$skip(33); 
  val nums = List(1, 2, 5, 4, 6);System.out.println("""nums  : List[Int] = """ + $show(nums ));$skip(64); 
  val diag3 = List(List(1, 0, 0), List(0, 1, 0), List(0, 0, 1));System.out.println("""diag3  : List[List[Int]] = """ + $show(diag3 ));$skip(21); 
  val empty = List();System.out.println("""empty  : List[Nothing] = """ + $show(empty ));$skip(29); 

  val pair = ("answer", 42);System.out.println("""pair  : (java.lang.String, Int) = """ + $show(pair ));$skip(28); 
  val (label, value) = pair;System.out.println("""label  : java.lang.String = """ + $show(label ));System.out.println("""value  : Int = """ + $show(value ));$skip(123); 

  def isort(xs: List[Int]): List[Int] = xs match {
    case List() => List()
    case y :: ys => insert(y, isort(ys))
  };System.out.println("""isort: (xs: List[Int])List[Int]""");$skip(156); 

  def insert(x: Int, xs: List[Int]): List[Int] = xs match {
    case List() => List(x)
    case y :: ys => if (x <= y) x :: xs else y :: insert(x, ys)
  };System.out.println("""insert: (x: Int, xs: List[Int])List[Int]""");$skip(168); 

  def init[T](xs: List[T]): List[T] = xs match {
    case List() => throw new Error("List is emply.")
    case List(x) => List()
    case y :: ys => y :: init(ys)
  };System.out.println("""init: [T](xs: List[T])List[T]""");$skip(134); 

  def concat[T](xs: List[T], ys: List[T]): List[T] = xs match {
    case List() => ys
    case z :: zs => z :: concat[T](zs, ys)
  };System.out.println("""concat: [T](xs: List[T], ys: List[T])List[T]""");$skip(157); 

  def reverse[T](xs: List[T]): List[T] = xs match {
    case List() => List() // or return xs
    case y :: ys => concat(reverse(ys), List(y)) // or ++
  };System.out.println("""reverse: [T](xs: List[T])List[T]""");$skip(73); 

  def removeAt(n: Int, xs: List[Int]) = (xs take n) ::: (xs drop n + 1);System.out.println("""removeAt: (n: Int, xs: List[Int])List[Int]""");$skip(15); val res$0 = 

  isort(nums);System.out.println("""res0: List[Int] = """ + $show(res$0));$skip(18); val res$1 = 
  insert(3, nums);System.out.println("""res1: List[Int] = """ + $show(res$1));$skip(13); val res$2 = 
  init(nums);System.out.println("""res2: List[Int] = """ + $show(res$2));$skip(21); val res$3 = 
  concat(nums, nums);System.out.println("""res3: List[Int] = """ + $show(res$3));$skip(16); val res$4 = 
  reverse(nums);System.out.println("""res4: List[Int] = """ + $show(res$4))}
}