import week3.Rational					// without this, have to call week3.Rational every time instead of just Rational

object scratch {

	//new week3.Rational(1,2)    //The would be the syntax without import above
	new Rational(1,2)
}