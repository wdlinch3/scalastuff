package week7

object primes {
  def from(n: Int): Stream[Int] = n #:: from(n+1) //> from: (n: Int)Stream[Int]
  
  val nat = from(0)                               //> nat  : Stream[Int] = Stream(0, ?)
  val N = from(1)                                 //> N  : Stream[Int] = Stream(1, ?)
  val m4s = nat map (_ * 4)                       //> m4s  : scala.collection.immutable.Stream[Int] = Stream(0, ?)
  
  (m4s take 10).toList                            //> res0: List[Int] = List(0, 4, 8, 12, 16, 20, 24, 28, 32, 36)
  
 	def sieve(s: Stream[Int]): Stream[Int] = {
 	  s.head #:: sieve(s.tail filter (_ % s.head !=0) )
 	}                                         //> sieve: (s: Stream[Int])Stream[Int]
 
 val primes = sieve(from(2))                      //> primes  : Stream[Int] = Stream(2, ?)
 
 (primes take 31).toList                          //> res1: List[Int] = List(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 4
                                                  //| 7, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127)
  /* Back to square roots */
  def sqrtStream(x: Double): Stream[Double] = {
  	def improve(guess: Double) = (guess + x/guess)/2
  	lazy val guesses: Stream[Double] = 1 #:: guesses map improve
  	guesses
  }                                               //> sqrtStream: (x: Double)Stream[Double]
  
  (sqrtStream(4) take 10).toList                  //> res2: List[Double] = List(2.5, 2.05, 2.000609756097561, 2.0000000929222947, 
                                                  //| 2.000000000000002, 2.0, 2.0, 2.0, 2.0, 2.0)
  
  def isGoodEnough(guess: Double, x: Double): Boolean = {
    math.abs( (guess*guess - x)/x ) < 0.0001
  }                                               //> isGoodEnough: (guess: Double, x: Double)Boolean
  
  sqrtStream(4).filter(isGoodEnough(_, 4))        //> res3: scala.collection.immutable.Stream[Double] = Stream(2.0000000929222947,
                                                  //|  ?)
  
}