package week04

//Base trait
trait List[+T] {
  def isEmpty: Boolean
  def head: T
  def tail: List[T]

// More practice with covariance. Consider the following example prepending elements to a list:
//  def prepend(elem: T): List[T] = new Cons( elem, this)
// This violates covariance since elem is a contravariant variable
// To fix it, we must pass it covariantly and supertype it as follows:
  def prepend[U >: T](elem: U): List[U] = new Cons(elem, this)
// We can check how this works on the function f defined in "test" below.  
  
}
//Implementations
class Cons[T](val head: T, val tail: List[T]) extends List[T] {
  def isEmpty = false
  //Note that head and tail are already implemented 
}

/* 
 * class Nil[T] extends List[T] {
 * def isEmpty: Boolean = true
 * def head: Nothing = throw new NoSuchElementException("Nil.head")
 * def tail: Nothing = throw new NoSuchElementException("Nil.tail")
 * }
 * Since there is only one Nil, it would be more elegant to make this into an object:
 */

object Nil extends List[Nothing] {
  def isEmpty: Boolean = true
  def head: Nothing = throw new NoSuchElementException("Nil.head")
  def tail: Nothing = throw new NoSuchElementException("Nil.tail")
}
// This is fine except that the following check would fail
object test {
  val x: List[String] = Nil
//  def f(xs: List[NonEmpty], x: Empty) = xs prepend x
// Cannot use this in this package: Need package IntSet  
}
// The error suggests that we solve this problem by making List[T] covariant in T, 
// that is, write List[+T], which we have done.
// By the way, we had to change all instances of "new Nil[T]" to "Nil" since objects 
// are unique (no "new" required).

object List {
  //List() = List.apply()
  def apply[T](): List[T] = Nil
  //List(1) = List.apply(1)
  def apply[T](x: T): List[T] = new Cons[T](x, Nil)
  //List(1, 2) = List.apply(1, 2)
  def apply[T](x1: T, x2: T): List[T] = new Cons[T](x1, new Cons[T](x2, Nil ) )
}

















 