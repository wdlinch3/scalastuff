package week04

/* ---------------------------*/
/* ---------------------------*/
/* ---------------------------*/
/* Peano numbers: Construction of non-negative integers without using primitive types */
/* The exception is the use of Boolean but we have seen that these too can be */
/* implemented abstractly as has been done above */

abstract class Nat {
  def isZero: Boolean 
  def predecessor: Nat
  def successor: Nat = new Succ(this)
  def + (that: Nat): Nat 
  def - (that: Nat): Nat
}

object Zero extends Nat {
  def isZero: Boolean = true
  def predecessor: Nat = throw new Exception ("There is no Natural less than Zero.")
//  def successor = new Succ(this)
  def + (that: Nat) = that
//  def - (that: Nat) = if (that==Zero) Zero else throw new Exception ("There is no Natural less than Zero.")
// 	This is "wrong" in the sense that we did not define ==. Should change to:   
  def - (that: Nat) = if (that.isZero) Zero else throw new Exception ("There is no Natural less than Zero.")
  
  override def toString = "0"
}

class Succ(n: Nat) extends Nat {
  def isZero: Boolean = false
//def predecessor: Nat = if (n.isZero) throw new Exception ("There is no Natural less than Zero.") else n  //This is wrong  
  def predecessor: Nat = n
//  def successor: Nat = new Succ(this)
//  def + (that: Nat): Nat = new Succ( this.predecessor + that ) Note that this is the same as
  def + (that: Nat): Nat = new Succ(n + that)
//  def - (that: Nat): Nat = if (that==Zero) this else (new Succ (n - that.predecessor)).predecessor
//  Again: avoid == using .isZero
//  def - (that: Nat): Nat = if (that.isZero) this else (new Succ (n - that.predecessor)).predecessor
// Dumbass: Succ().predecessor is just ():  
  def - (that: Nat): Nat = if (that.isZero) this else n - that.predecessor
  
  override def toString = "["+ this.predecessor +"]"
} 