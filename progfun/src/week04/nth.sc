import week04._

object nth {
  def nth[T](n: Int, xs: List[T]): T = {
    if (xs.isEmpty) throw new IndexOutOfBoundsException
    else if (n == 0) xs.head
    else nth(n - 1, xs.tail)
  }                                               //> nth: [T](n: Int, xs: week04.List[T])T

  val list = new Cons(1, new Cons(2, new Cons(3, Nil)))
                                                  //> list  : week04.Cons[Int] = week04.Cons@ac980c9
  nth(2, list)                                    //> res0: Int = 3
//  nth(4, list)
//  nth(-1, list)

//	val zero = new Zero is WRONG: Cannot make new instances of object
	val zero = Zero                           //> zero  : week04.Zero.type = 0
	val one = new Succ(Zero)                  //> one  : week04.Succ = [0]
	val two = one.successor                   //> two  : week04.Nat = [[0]]
	val three = two.successor                 //> three  : week04.Nat = [[[0]]]
//
	zero + one                                //> res1: week04.Nat = [0]
	one + zero                                //> res2: week04.Nat = [0]
//
	zero + two                                //> res3: week04.Nat = [[0]]
	two + zero                                //> res4: week04.Nat = [[0]]
	one + one                                 //> res5: week04.Nat = [[0]]
//
	zero + three                              //> res6: week04.Nat = [[[0]]]
	three + zero                              //> res7: week04.Nat = [[[0]]]
	one + two                                 //> res8: week04.Nat = [[[0]]]
	two + one                                 //> res9: week04.Nat = [[[0]]]
	one + one + one                           //> res10: week04.Nat = [[[0]]]
	one + two                                 //> res11: week04.Nat = [[[0]]]
//
  one + one + two + two                           //> res12: week04.Nat = [[[[[[0]]]]]]
	one + two + three                         //> res13: week04.Nat = [[[[[[0]]]]]]
//
	one - one                                 //> res14: week04.Nat = 0
	two - two                                 //> res15: week04.Nat = 0
	three - three                             //> res16: week04.Nat = 0
//
	one - two                                 //> java.lang.Exception: There is no Natural less than Zero.
                                                  //| 	at week04.Zero$.$minus(Nat.scala:25)
                                                  //| 	at week04.Succ.$minus(Nat.scala:41)
                                                  //| 	at nth$$anonfun$main$1.apply$mcV$sp(nth.scala:42)
                                                  //| 	at org.scalaide.worksheet.runtime.library.WorksheetSupport$$anonfun$$exe
                                                  //| cute$1.apply$mcV$sp(WorksheetSupport.scala:76)
                                                  //| 	at org.scalaide.worksheet.runtime.library.WorksheetSupport$.redirected(W
                                                  //| orksheetSupport.scala:65)
                                                  //| 	at org.scalaide.worksheet.runtime.library.WorksheetSupport$.$execute(Wor
                                                  //| ksheetSupport.scala:75)
                                                  //| 	at nth$.main(nth.scala:3)
                                                  //| 	at nth.main(nth.scala)
	one - three
}