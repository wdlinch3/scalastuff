package week6

object pairs {

  val n = 7                                       //> n  : Int = 7

  val xss = (1 until n) map (i =>
    ((1 until i) map (j => (i, j))))              //> xss  : scala.collection.immutable.IndexedSeq[scala.collection.immutable.Inde
                                                  //| xedSeq[(Int, Int)]] = Vector(Vector(), Vector((2,1)), Vector((3,1), (3,2)), 
                                                  //| Vector((4,1), (4,2), (4,3)), Vector((5,1), (5,2), (5,3), (5,4)), Vector((6,1
                                                  //| ), (6,2), (6,3), (6,4), (6,5)))
  //This command flattens the list
  //(xss foldRight Seq[(Int, Int)]() ) (_ ++ _)
  //But there is the following shorthand for it:
  xss.flatten                                     //> res0: scala.collection.immutable.IndexedSeq[(Int, Int)] = Vector((2,1), (3,1
                                                  //| ), (3,2), (4,1), (4,2), (4,3), (5,1), (5,2), (5,3), (5,4), (6,1), (6,2), (6,
                                                  //| 3), (6,4), (6,5))
  //The shorter version of all of this uses
  //(xs flatMap f) = (xs map f).flatten
  //So, we can write
  (1 until n) flatMap (i =>
    ((1 until i) map (j => (i, j))))              //> res1: scala.collection.immutable.IndexedSeq[(Int, Int)] = Vector((2,1), (3,1
                                                  //| ), (3,2), (4,1), (4,2), (4,3), (5,1), (5,2), (5,3), (5,4), (6,1), (6,2), (6,
                                                  //| 3), (6,4), (6,5))
  //To filter on whether the sum of the pairs is prime we use the previous definition of isPrime
  def isPrime(n: Int): Boolean = (2 until n).forall(c => n % c != 0)
                                                  //> isPrime: (n: Int)Boolean
  //and filter the result above
  (1 until n) flatMap (i =>
    ((1 until i) map (j => (i, j)))) filter { case (x, y) => isPrime(x + y) }
                                                  //> res2: scala.collection.immutable.IndexedSeq[(Int, Int)] = Vector((2,1), (3,2
                                                  //| ), (4,1), (4,3), (5,2), (6,1), (6,5))
  //Same implementation with for syntax
  for {
    i <- 1 until n
    j <- i until i
    if isPrime(i + j)
  } yield (i, j)                                  //> res3: scala.collection.immutable.IndexedSeq[(Int, Int)] = Vector()

  def dumbScalarProduct(v1: List[Double], v2: List[Double]): Double = (for (i <- 0 until v1.length) yield v1(i) * v2(i)).sum
                                                  //> dumbScalarProduct: (v1: List[Double], v2: List[Double])Double
  
  //A cleverer implementation uses zip:
  def scalarProduct(v1: List[Double], v2: List[Double]): Double = ( for ( (x, y) <- v1 zip v2 ) yield (x * y) ).sum
                                                  //> scalarProduct: (v1: List[Double], v2: List[Double])Double

}