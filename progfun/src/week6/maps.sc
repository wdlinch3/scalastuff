package week6

object maps {
  
  val romanNumerals = Map("I" -> 1, "V" -> 5, "X"-> 10)
                                                  //> romanNumerals  : scala.collection.immutable.Map[java.lang.String,Int] = Map(I
                                                  //|  -> 1, V -> 5, X -> 10)
  val capitalOfCountry = Map("US" -> "Washington" , "Switzerland" -> "Bern")
                                                  //> capitalOfCountry  : scala.collection.immutable.Map[java.lang.String,java.lan
                                                  //| g.String] = Map(US -> Washington, Switzerland -> Bern)
  
//	capitalOfCountry("US")
//	capitalOfCountry("Andorra")
//Better:
	capitalOfCountry get "US"                 //> res0: Option[java.lang.String] = Some(Washington)
	capitalOfCountry get "Andorra"            //> res1: Option[java.lang.String] = None
}