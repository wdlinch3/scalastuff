package week6

object test {

  val xs = Array(1, 2, 3, 44)                     //> xs  : Array[Int] = Array(1, 2, 3, 44)
  xs map (x => x * 2)                             //> res0: Array[Int] = Array(2, 4, 6, 88)

  val s = "Hello World"                           //> s  : java.lang.String = Hello World
  s filter (c => c.isUpper)                       //> res1: String = HW

  s exists (c => c.isUpper)                       //> res2: Boolean = true
  s forall (c => c.isUpper)                       //> res3: Boolean = false
  val pairs = List(1, 2, 3) zip s                 //> pairs  : List[(Int, Char)] = List((1,H), (2,e), (3,l))
  pairs.unzip                                     //> res4: (List[Int], List[Char]) = (List(1, 2, 3),List(H, e, l))

  s flatMap (c => List('.', c))                   //> res5: String = .H.e.l.l.o. .W.o.r.l.d

  xs.sum                                          //> res6: Int = 50
  xs.max                                          //> res7: Int = 44

  //All (x,y) with x in 1,..,4 and y in 1,..,3
  (1 to 4) flatMap (x => (1 to 3) map (y => (x, y)))
                                                  //> res8: scala.collection.immutable.IndexedSeq[(Int, Int)] = Vector((1,1), (1,2
                                                  //| ), (1,3), (2,1), (2,2), (2,3), (3,1), (3,2), (3,3), (4,1), (4,2), (4,3))

  //  def scalarProduct( v1: Vector[Double], v2: Vector[Double] ): Double = {
  //    (v1 zip v2).map( x => x._1 * x._2 ).sum

  def scalarProduct(v1: Vector[Double], v2: Vector[Double]): Double = {
    (v1 zip v2).map { case (x, y) => x * y }.sum

    // which is an abbreviation for
    //	def scalarProduct( v1: Vector[Double], v2: Vector[Double] ): Double = {
    //    (v1 zip v2).map( x => x match { case (x, y) => x * y } ).sum
  }                                               //> scalarProduct: (v1: Vector[Double], v2: Vector[Double])Double

  //  def isPrime( n: Int ): Boolean = !( (2 to n-1).exists(c => (n%c==0)) )
  def isPrime(n: Int): Boolean = (2 until n).forall( c => n % c != 0 )
                                                  //> isPrime: (n: Int)Boolean

  (1 to 15).map(c => isPrime(c))                  //> res9: scala.collection.immutable.IndexedSeq[Boolean] = Vector(true, true, t
                                                  //| rue, false, true, false, true, false, false, false, true, false, true, fals
                                                  //| e, false)

}