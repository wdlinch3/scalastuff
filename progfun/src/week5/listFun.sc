package week5

object listFun {
  val nums = List(2, -4, 5, 7, 1)                 //> nums  : List[Int] = List(2, -4, 5, 7, 1)
  val fruits = List("apple", "pineapple", "orange", "banana")
                                                  //> fruits  : List[java.lang.String] = List(apple, pineapple, orange, banana)
  val repeats = List("a", "a", "a", "b", "b", "c", "a")
                                                  //> repeats  : List[java.lang.String] = List(a, a, a, b, b, c, a)

  nums filter (x => x > 0)                        //> res0: List[Int] = List(2, 5, 7, 1)
  nums filterNot (x => x > 0)                     //> res1: List[Int] = List(-4)
  nums partition (x => x > 0)                     //> res2: (List[Int], List[Int]) = (List(2, 5, 7, 1),List(-4))

  nums takeWhile (x => x > 0)                     //> res3: List[Int] = List(2)
  nums dropWhile (x => x > 0)                     //> res4: List[Int] = List(-4, 5, 7, 1)
  nums span (x => x > 0)                          //> res5: (List[Int], List[Int]) = (List(2),List(-4, 5, 7, 1))

  def pack[T](xs: List[T]): List[List[T]] = xs match {
    case Nil => Nil
    case y :: ys => //List(List(y), pack( ys.dropWhile(y => (y==ys.head)) ))
      val (first, rest) = xs span (x => x == y)
      first :: pack(rest)
  }                                               //> pack: [T](xs: List[T])List[List[T]]

  pack(repeats)                                   //> res6: List[List[java.lang.String]] = List(List(a, a, a), List(b, b), List(c)
                                                  //| , List(a))

  /*	def encode[T](xs: List[T]): List[(T, Int)] = {
		encode0[T](ys: List[List[T]]): List[(T, Int)]= ys match {
			case Nil => Nil
			case z::zs => (z.head, z.length)::encode0(zs)
			}
		encode0( pack(xs))
	}
*/

  /*  def encode[T](xs: List[T]): List[(T,Int)] = xs match {
    case Nil => Nil
    case y :: ys =>
      val (first, rest) = xs span (x => x == y)
      (first.head, first.length) :: encode(rest)
  }
*/

  def encode[T](xs: List[T]): List[(T, Int)] = pack(xs) map (ys => (ys.head, ys.length))
                                                  //> encode: [T](xs: List[T])List[(T, Int)]

  encode(repeats)                                 //> res7: List[(java.lang.String, Int)] = List((a,3), (b,2), (c,1), (a,1))

  def concat[T](xs: List[T], ys: List[T]): List[T] =
    (xs foldRight ys)(_ :: _)                     //> concat: [T](xs: List[T], ys: List[T])List[T]

  def reverse[T](xs: List[T]): List[T] =
    (xs foldLeft List[T]())((x, y) => y :: x)     //> reverse: [T](xs: List[T])List[T]

  reverse(nums)                                   //> res8: List[Int] = List(1, 7, 5, -4, 2)

  def mapFun[T, U](xs: List[T], f: T => U): List[U] = (xs foldRight List[U]())( (x,y) => f(x)::y)
                                                  //> mapFun: [T, U](xs: List[T], f: T => U)List[U]
	mapFun[Int,Int](nums , x=> x*x )          //> res9: List[Int] = List(4, 16, 25, 49, 1)

  def lengthFun[T](xs: List[T]): Int = (xs foldRight 0)( (x,y) => y+1  )
                                                  //> lengthFun: [T](xs: List[T])Int
  lengthFun[Int](nums)                            //> res10: Int = 5
}