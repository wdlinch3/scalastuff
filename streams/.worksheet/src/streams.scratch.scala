package streams

import streams._
import common._
import Bloxorz._
import scala.math.BigInt

//import GameDef._


object scratch {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(200); 

  val level =
    """ST
           |oo
           |oo""".stripMargin;System.out.println("""level  : String = """ + $show(level ));$skip(18); 

  println(level);$skip(83); 
  val levelToVector = Vector(Vector('S', 'T'), Vector('o', '-'), Vector('o', 'o'));System.out.println("""levelToVector  : scala.collection.immutable.Vector[scala.collection.immutable.Vector[Char]] = """ + $show(levelToVector ));$skip(19); val res$0 = 
  levelToVector(0);System.out.println("""res0: scala.collection.immutable.Vector[Char] = """ + $show(res$0));$skip(29); val res$1 = 
  levelToVector(1)(1) == '-';System.out.println("""res1: Boolean = """ + $show(res$1));$skip(22); val res$2 = 
  levelToVector(0)(1);System.out.println("""res2: Char = """ + $show(res$2));$skip(19); val res$3 = 
  levelToVector(1);System.out.println("""res3: scala.collection.immutable.Vector[Char] = """ + $show(res$3));$skip(19); val res$4 = 
  levelToVector(2);System.out.println("""res4: scala.collection.immutable.Vector[Char] = """ + $show(res$4));$skip(94); 
  //levelToVector(3)

  val lookForC =
    """ST
           |oc
           |oo""".stripMargin;System.out.println("""lookForC  : String = """ + $show(lookForC ));$skip(18); 

  println(level);$skip(84); 
  val lookForCVector = Vector(Vector('S', 'T'), Vector('o', 'o'), Vector('o', 'c'));System.out.println("""lookForCVector  : scala.collection.immutable.Vector[scala.collection.immutable.Vector[Char]] = """ + $show(lookForCVector ));$skip(49); val res$5 = 
  for (v <- lookForCVector) yield v.indexOf('c');System.out.println("""res5: scala.collection.immutable.Vector[Int] = """ + $show(res$5));$skip(62); val res$6 = 
  (for (v <- lookForCVector) yield v.indexOf('c')).indexOf(1);System.out.println("""res6: Int = """ + $show(res$6));$skip(64); 

  val xcoord = lookForCVector.indexWhere(v => v.contains('c'));System.out.println("""xcoord  : Int = """ + $show(xcoord ));$skip(51); 
  val ycoord = lookForCVector(xcoord).indexOf('c');System.out.println("""ycoord  : Int = """ + $show(ycoord ));$skip(308); val res$7 = 

  // val coordsC =  List( lookForCVector.indexWhere(v => v.contains('c')), lookForCVector.indexWhere(v => v.contains('c')).indexWhere(v => v.contains('c')) )

// Can't figure out how to get the worksheet to recognize this function defined in StringParserTerrain
//  findChar("c", lookForCVector)

BigInt(0);System.out.println("""res7: scala.math.BigInt = """ + $show(res$7));$skip(210); 


//  val fibs: Stream[BigInt] = BigInt(0) #:: BigInt(1) #:: fibs.zip(fibs.tail).map( x => x._1 + x._2 )
lazy val fibs: Stream[BigInt] = BigInt(0) #:: BigInt(1) #:: fibs.zip(fibs.tail).map { n => n._1 + n._2 };System.out.println("""fibs  : Stream[scala.math.BigInt] = <lazy>""");$skip(33); 
     val seventh =  fibs.take(3);System.out.println("""seventh  : scala.collection.immutable.Stream[scala.math.BigInt] = """ + $show(seventh ));$skip(26); val res$8 = 
     
     seventh.toList;System.out.println("""res8: List[scala.math.BigInt] = """ + $show(res$8));$skip(38); 
     
      val first = fibs zip fibs;System.out.println("""first  : scala.collection.immutable.Stream[(scala.math.BigInt, scala.math.BigInt)] = """ + $show(first ));$skip(15); val res$9 = 
      first(0);System.out.println("""res9: (scala.math.BigInt, scala.math.BigInt) = """ + $show(res$9));$skip(15); val res$10 = 
      first(1);System.out.println("""res10: (scala.math.BigInt, scala.math.BigInt) = """ + $show(res$10));$skip(15); val res$11 = 
      first(2);System.out.println("""res11: (scala.math.BigInt, scala.math.BigInt) = """ + $show(res$11));$skip(15); val res$12 = 
      first(3);System.out.println("""res12: (scala.math.BigInt, scala.math.BigInt) = """ + $show(res$12));$skip(17); val res$13 = 
     
seventh(1);System.out.println("""res13: scala.math.BigInt = """ + $show(res$13));$skip(11); val res$14 = 
seventh(2);System.out.println("""res14: scala.math.BigInt = """ + $show(res$14));$skip(11); val res$15 = 
seventh(3);System.out.println("""res15: scala.math.BigInt = """ + $show(res$15));$skip(11); val res$16 = 
seventh(4);System.out.println("""res16: scala.math.BigInt = """ + $show(res$16));$skip(24); 

  val zeroSet = Set(0);System.out.println("""zeroSet  : scala.collection.immutable.Set[Int] = """ + $show(zeroSet ));$skip(20); val res$17 = 
  Set(1) ++ zeroSet;System.out.println("""res17: scala.collection.immutable.Set[Int] = """ + $show(res$17))}
  
 
  
  
  
}