package streams

import streams._
import common._
import Bloxorz._
import scala.math.BigInt

//import GameDef._


object scratch {

  val level =
    """ST
           |oo
           |oo""".stripMargin                     //> level  : String = ST
                                                  //| oo
                                                  //| oo

  println(level)                                  //> ST
                                                  //| oo
                                                  //| oo
  val levelToVector = Vector(Vector('S', 'T'), Vector('o', '-'), Vector('o', 'o'))
                                                  //> levelToVector  : scala.collection.immutable.Vector[scala.collection.immutabl
                                                  //| e.Vector[Char]] = Vector(Vector(S, T), Vector(o, -), Vector(o, o))
  levelToVector(0)                                //> res0: scala.collection.immutable.Vector[Char] = Vector(S, T)
  levelToVector(1)(1) == '-'                      //> res1: Boolean = true
  levelToVector(0)(1)                             //> res2: Char = T
  levelToVector(1)                                //> res3: scala.collection.immutable.Vector[Char] = Vector(o, -)
  levelToVector(2)                                //> res4: scala.collection.immutable.Vector[Char] = Vector(o, o)
  //levelToVector(3)

  val lookForC =
    """ST
           |oc
           |oo""".stripMargin                     //> lookForC  : String = ST
                                                  //| oc
                                                  //| oo

  println(level)                                  //> ST
                                                  //| oo
                                                  //| oo
  val lookForCVector = Vector(Vector('S', 'T'), Vector('o', 'o'), Vector('o', 'c'))
                                                  //> lookForCVector  : scala.collection.immutable.Vector[scala.collection.immutab
                                                  //| le.Vector[Char]] = Vector(Vector(S, T), Vector(o, o), Vector(o, c))
  for (v <- lookForCVector) yield v.indexOf('c')  //> res5: scala.collection.immutable.Vector[Int] = Vector(-1, -1, 1)
  (for (v <- lookForCVector) yield v.indexOf('c')).indexOf(1)
                                                  //> res6: Int = 2

  val xcoord = lookForCVector.indexWhere(v => v.contains('c'))
                                                  //> xcoord  : Int = 2
  val ycoord = lookForCVector(xcoord).indexOf('c')//> ycoord  : Int = 1

  // val coordsC =  List( lookForCVector.indexWhere(v => v.contains('c')), lookForCVector.indexWhere(v => v.contains('c')).indexWhere(v => v.contains('c')) )

// Can't figure out how to get the worksheet to recognize this function defined in StringParserTerrain
//  findChar("c", lookForCVector)

BigInt(0)                                         //> res7: scala.math.BigInt = 0


//  val fibs: Stream[BigInt] = BigInt(0) #:: BigInt(1) #:: fibs.zip(fibs.tail).map( x => x._1 + x._2 )
lazy val fibs: Stream[BigInt] = BigInt(0) #:: BigInt(1) #:: fibs.zip(fibs.tail).map { n => n._1 + n._2 }
                                                  //> fibs  : Stream[scala.math.BigInt] = <lazy>
     val seventh =  fibs.take(3)                  //> seventh  : scala.collection.immutable.Stream[scala.math.BigInt] = Stream(0,
                                                  //|  ?)
     
     seventh.toList                               //> res8: List[scala.math.BigInt] = List(0, 1, 1)
     
      val first = fibs zip fibs                   //> first  : scala.collection.immutable.Stream[(scala.math.BigInt, scala.math.B
                                                  //| igInt)] = Stream((0,0), ?)
      first(0)                                    //> res9: (scala.math.BigInt, scala.math.BigInt) = (0,0)
      first(1)                                    //> res10: (scala.math.BigInt, scala.math.BigInt) = (1,1)
      first(2)                                    //> res11: (scala.math.BigInt, scala.math.BigInt) = (1,1)
      first(3)                                    //> res12: (scala.math.BigInt, scala.math.BigInt) = (2,2)
     
seventh(1)                                        //> res13: scala.math.BigInt = 1
seventh(2)                                        //> res14: scala.math.BigInt = 1
seventh(3)                                        //> java.lang.IndexOutOfBoundsException: 3
                                                  //| 	at scala.collection.LinearSeqOptimized$class.apply(LinearSeqOptimized.sc
                                                  //| ala:51)
                                                  //| 	at scala.collection.immutable.Stream.apply(Stream.scala:186)
                                                  //| 	at streams.scratch$$anonfun$main$1.apply$mcV$sp(streams.scratch.scala:62
                                                  //| )
                                                  //| 	at org.scalaide.worksheet.runtime.library.WorksheetSupport$$anonfun$$exe
                                                  //| cute$1.apply$mcV$sp(WorksheetSupport.scala:76)
                                                  //| 	at org.scalaide.worksheet.runtime.library.WorksheetSupport$.redirected(W
                                                  //| orksheetSupport.scala:65)
                                                  //| 	at org.scalaide.worksheet.runtime.library.WorksheetSupport$.$execute(Wor
                                                  //| ksheetSupport.scala:75)
                                                  //| 	at streams.scratch$.main(streams.scratch.scala:11)
                                                  //| 	at streams.scratch.main(streams.scratch.scala)
seventh(4)

  val zeroSet = Set(0)
  Set(1) ++ zeroSet
  
 
  
  
  
}