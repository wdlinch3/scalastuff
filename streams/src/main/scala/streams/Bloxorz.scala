package streams

/**
 * A main object that can be used to execute the Bloxorz solver
 */
object Bloxorz extends App {

  /**
   * A level constructed using the `InfiniteTerrain` trait which defines
   * the terrain to be valid at every position.
   */
  object InfiniteLevel extends Solver with InfiniteTerrain {
    val startPos = Pos(1,3)
    val goal = Pos(5,8)
  }
  println
  println("Infinite level solution:")
  println(InfiniteLevel.solution)
  

  /**
   * A simple level constructed using the StringParserTerrain 
   */
  abstract class Level extends Solver with StringParserTerrain
  
  object Level0 extends Level {
    val level =
      """------
        |--ST--
        |--oo--
        |--oo--
        |------""".stripMargin
  }

  println
  println("Level 0 solution:")
  println(Level0.solution)

  /**
   * Level 1 of the official Bloxorz game
   */
  object Level1 extends Level {
    val level =
      """ooo-------
        |oSoooo----
        |ooooooooo-
        |-ooooooooo
        |-----ooToo
        |------ooo-""".stripMargin
  }
  println
  println("Level 1 solution:")
  println(Level1.solution)

  object Level2 extends Level {
    val level =
      """ooo-------
        |---ooooooo
        |oS-ooo----
        |oooooo-oo-
        |-ooo-o-ooo
        |-----ooToo
        |------ooo-""".stripMargin
  }
  println
  println("Level 2 solution:")
  println(Level2.solution)

  object Level3 extends Level {
    val level =
      """------ooooooo--
        |oooo--ooo--oo--
        |ooooooooo--oooo
        |oSoo-------ooTo
        |oooo-------oooo
        |------------ooo
        |---------------""".stripMargin
  }
  println
  println("Level 3 solution:")
  println(Level3.solution)

  
}
