package streams

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import Bloxorz._

@RunWith(classOf[JUnitRunner])
class BloxorzSuite extends FunSuite {

  trait SolutionChecker extends GameDef with Solver with StringParserTerrain {
    /**
     * This method applies a list of moves `ls` to the block at position
     * `startPos`. This can be used to verify if a certain list of moves
     * is a valid solution, i.e. leads to the goal.
     */
    def solve(ls: List[Move]): Block =
      ls.foldLeft(startBlock) {
        case (block, move) => move match {
          case Left => block.left
          case Right => block.right
          case Up => block.up
          case Down => block.down
        }
      }
  }

  trait Level1 extends SolutionChecker {
    /* terrain for level 1*/

    val level =
      """ooo-------
        |oSoooo----
        |ooooooooo-
        |-ooooooooo
        |-----ooToo
        |------ooo-""".stripMargin

    val optsolution = List(Right, Right, Down, Right, Right, Right, Down)
  }

//  trait Level2 extends SolutionChecker {
//    /* terrain for level 1*/
//
//    val level =
//      """ooo-------
//        |oSoooo----
//        |ooooooooo-
//        |-ooooooooo
//        |-----ooToo
//        |------ooo-""".stripMargin
//
//    val optsolution = ??? //List(Right, Right, Down, Right, Right, Right, Down)
//  }
  
  test("terrainFunction test") {
    new Level1 {
      val vector: Vector[Vector[Char]] = Vector(level.split("\n").map(str => Vector(str: _*)): _*)
      assert(terrainFunction(vector)(Pos(0, 0)) === true)
    }
  }

  test("terrain function level 1") {
    new Level1 {
      //assert(terrain(Pos(0,0)), "0,0")
      //assert(!terrain(Pos(4, 11)), "4,11")
    }
  }

  test("findChar level 1") {
    new Level1 {
      assert(startPos == Pos(1, 1))
    }
  }

  test("some moves in level 1") {
    new Level1 {
      val startingBlock = Block(Pos(1, 1), Pos(1, 1))
      assert(startBlock.right == Block(Pos(1, 2), Pos(1, 3)))
    }
  }

  test("Checking legal neighbors level 1") {
    new Level1 {
      val almostSafeBlock = Block(Pos(2, 2), Pos(2, 2))
      assert(almostSafeBlock.legalNeighbors.toSet ===
        Set(
          (Block(Pos(0, 2), Pos(1, 2)), Up),
          (Block(Pos(2, 0), Pos(2, 1)), Left),
          (Block(Pos(2, 3), Pos(2, 4)), Right)))
    }
  }

  test("Checking neighbor functions") {
    new Level1 {
      val startingBlock = Block(Pos(1, 1), Pos(1, 1))
      val safeBlock = Block(Pos(1, 1), Pos(1, 2))
      //      assert(startingBlock.neighbors.toSet === 
      //        Set(
      //            (Block(Pos(0,1), Pos(-1,1)), Up),
      //            (Block(Pos(2,1), Pos(3,1)), Down),
      //            (Block(Pos(1,-1), Pos(1,0)), Left),
      //            (Block(Pos(1,2), Pos(1,3)), Right)
      //        )
      //      )
      assert(startingBlock.legalNeighbors.toSet ===
        Set(
          (Block(Pos(2, 1), Pos(3, 1)), Down),
          (Block(Pos(1, 2), Pos(1, 3)), Right)))
      assert(safeBlock.neighbors.toSet ===
        Set(
          (Block(Pos(0, 1), Pos(0, 2)), Up),
          (Block(Pos(2, 1), Pos(2, 2)), Down),
          (Block(Pos(1, 0), Pos(1, 0)), Left),
          (Block(Pos(1, 3), Pos(1, 3)), Right)))
    }
  }

  test("finding Neighbors with history") {
    new Level1 {
      assert(neighborsWithHistory(Block(Pos(1, 1), Pos(1, 1)), List(Left, Up)).toSet ===
        Set(
          (Block(Pos(1, 2), Pos(1, 3)), List(Right, Left, Up)),
          (Block(Pos(2, 1), Pos(3, 1)), List(Down, Left, Up))))
    }
  }

  test("finding Neighbors with history 2") {
    new Level1 {
      assert(neighborsWithHistory(Block(Pos(1, 2), Pos(1, 3)), List(Right)).toSet ===
        Set(
          (Block(Pos(1, 4), Pos(1, 4)), List(Right, Right)),
          (Block(Pos(1, 1), Pos(1, 1)), List(Left, Right)),
          (Block(Pos(2, 2), Pos(2, 3)), List(Down, Right))))
    }
  }

  test("Testing newNeighborsOnly") {
    new Level1 {
      val somePath = Set(
        (Block(Pos(1, 2), Pos(1, 3)), List(Right, Left, Up)),
        (Block(Pos(2, 1), Pos(3, 1)), List(Down, Left, Up))).toStream
      val someHistory = Set(
        Block(Pos(1, 2), Pos(1, 3)),
        Block(Pos(1, 1), Pos(1, 1)))
      assert(
        newNeighborsOnly(somePath, someHistory) ===
          Set(
            (Block(Pos(2, 1), Pos(3, 1)), List(Down, Left, Up))).toStream)
    }
  }

//  test("Testing from take 12") {
//    new Level1 {
//      assert(from(Stream((Block(startPos, startPos), Nil)), Set(Block(startPos, startPos))).take(100).toList ===
//        (Block(startPos, startPos), Nil))
//    }
//  }

//  test("Testing from step X") {
//    new Level1 {
//      assert(from(Stream((Block(startPos, startPos), Nil)), Set(Block(startPos, startPos)))(11) ===
//        (Block(startPos, startPos), Nil))
//    }
//  }

  test("Testing pathsFromStart") {
    new Level1 {
      assert(pathsFromStart.take(11).toList ===
        from(Stream((Block(startPos, startPos), Nil)), Set(Block(startPos, startPos))).take(11).toList)
    }
  }

//  test("Testing pathsToGoal") {
//    new Level1 {
//      assert(pathsToGoal.take(10).toList === List('1'))
//    }
//  }

//  test("Testing pathsToGoal") {
//    new Level1 {
//      assert( solution === List('1'))
//    }
//  }

  test("optimal solution for level 1") {
    new Level1 {
      assert(solve(solution) === Block(goal, goal))
    }
  }

  test("optimal solution length for level 1") {
    new Level1 {
      assert(solution.length === optsolution.length)
    }
  }
}
