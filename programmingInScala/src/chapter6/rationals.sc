package chapter6

//import chapter6._

object rationals {

	val oneHalf = new Rational(1,2)           //> oneHalf  : chapter6.Rational = 1/2
	val oneThird = new Rational(1,3)          //> oneThird  : chapter6.Rational = 1/3
	val oneSixth = new Rational(1,6)          //> oneSixth  : chapter6.Rational = 1/6
	val twoThird = new Rational(2,3)          //> twoThird  : chapter6.Rational = 2/3
	
	val someSum = oneHalf + oneThird + oneSixth
                                                  //> someSum  : chapter6.Rational = 1
	val someRat = new Rational(66,42)         //> someRat  : chapter6.Rational = 11/7
	val oneSixthPrime = oneHalf*oneThird      //> oneSixthPrime  : chapter6.Rational = 1/6
	//Natural equality
	oneSixthPrime == oneSixth                 //> res0: Boolean = false
	//vs reference equality
	oneSixthPrime equals oneSixth             //> res1: Boolean = false
	
	oneHalf + 2                               //> res2: chapter6.Rational = 5/2
	//But can't yet do 2 + oneThird

	oneHalf * 2                               //> res3: chapter6.Rational = 1
	oneHalf/oneSixth                          //> res4: chapter6.Rational = 3

}