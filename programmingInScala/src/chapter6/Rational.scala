package chapter6

class Rational(n: Int, d: Int) {

  //Non-vanishing denominator
  require(d != 0)

  //Need to make numerator and denominator into fields
  //To implement the reduced form, we need to remove the gcd
  private val g = gcd(n.abs, d.abs)
  //This function is defined below
  val numerator = n / g
  val denominator = d / g

  //Auxiliary constructor for rationals with denominator=1
  def this(n: Int) = this(n, 1)

  //Addition: On proper rationals
  def + (that: Rational): Rational =
    new Rational(n * that.denominator + that.numerator * d, d * that.denominator)
  //On integers
  def + (that: Int): Rational =
    this + new Rational(that)
  
  //Unary - (this needs a space in unary_- :)
  def unary_- : Rational =
    new Rational( -this.numerator, denominator)
  
  //Subtraction
  def - (that: Rational): Rational =
    this + (-that)

  //Multiplication: On proper rationals
  def * (that: Rational): Rational =
    new Rational ( this.numerator*that.numerator, this.denominator*that.denominator)
  //On integers
  def * (that: Int): Rational =
    this * new Rational(that)
  //Note: This does not define Int.+(Rational) 
  //A temporary workaround is to use 
  //implicit def intToRational(x: Int) = new Rational(x)
  //Unfortunately, this needs to be in the scope of Int and not Rational
  //so this can only be used directly in the interpreter. 
  
  //Division 
  def / (that: Rational): Rational =
    this * new Rational(that.denominator, this.numerator)

  //Greatest Common Divisor as implemented by
  //Euclid's algorithm http://en.wikipedia.org/wiki/Greatest_common_divisor
  private def gcd(a: Int, b: Int): Int =
    if (b == 0) a else gcd(b, a % b)
    
   //Debugging line:
  //println("Created " + n + "/" + d)
  //Replace with toString override
  override def toString = {
      if (denominator == 1) numerator.toString
      else numerator + "/" + denominator
      }
  
}