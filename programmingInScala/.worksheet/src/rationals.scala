e chapter6

//import chapter6._

object rationals {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(85); 

	val oneHalf = new Rational(1,2)
	val oneThird = new Rational(1,3)
	val oneSixth =;System.out.println("""oneHalf  : Rational = """ + $show(oneHalf ));$skip(102);  new Rational(1,6)
	val twoThird = new Rational(2,3)
	
	oneHalf + oneThird + oneSixth;System.out.println("""oneThird  : Rational = """ + $show(oneThird ));$skip(93); 

	new Rational(66,42)


}

class Rational(n: Int, d: Int) {;System.out.println("""oneSixth  : Rational = """ + $show(oneSixth ));$skip(105); 

  //Non-vanishing denominator
  require(d != 0);System.out.println("""twoThird  : Rational = """ + $show(twoThird ));$skip(61); val res$0 = 

  //Need to make numerator and denominator into fields
  //To implement the reduced form, we need to remove the gcd
  private val g = gcd;System.out.println("""res0: Rational = """ + $show(res$0));$skip(71); val res$1 = (n.abs, d.abs)
  //This function is defined below;System.out.println("""res1: Rational = """ + $show(res$1))}
  val numerator = n / g
  val denominator = d / g

  //Auxiliary constructor for rationals with denominator=1
  def this(n: Int) = this(n, 1)

  //Addition
  def +(that: Rational): Rational =
    new Rational(n * that.denominator + that.numerator * d, d * that.denominator)

  //Debugging line:
  //println("Created " + n + "/" + d)
  //Replace with toString override
  override def toString = n + "/" + d

  //Greatest Common Divisor as implemented by
  //Euclid's algorithm http://en.wikipedia.org/wiki/Greatest_common_divisor
  private def gcd(a: Int, b: Int): Int =
    if (b == 0) a else gcd(b, a % b)
}