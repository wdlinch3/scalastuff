package chapter6

//import chapter6._

object rationals {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(91); 

	val oneHalf = new Rational(1,2);System.out.println("""oneHalf  : chapter6.Rational = """ + $show(oneHalf ));$skip(34); 
	val oneThird = new Rational(1,3);System.out.println("""oneThird  : chapter6.Rational = """ + $show(oneThird ));$skip(34); 
	val oneSixth = new Rational(1,6);System.out.println("""oneSixth  : chapter6.Rational = """ + $show(oneSixth ));$skip(34); 
	val twoThird = new Rational(2,3);System.out.println("""twoThird  : chapter6.Rational = """ + $show(twoThird ));$skip(47); 
	
	val someSum = oneHalf + oneThird + oneSixth;System.out.println("""someSum  : chapter6.Rational = """ + $show(someSum ));$skip(35); 
	val someRat = new Rational(66,42);System.out.println("""someRat  : chapter6.Rational = """ + $show(someRat ));$skip(38); 
	val oneSixthPrime = oneHalf*oneThird;System.out.println("""oneSixthPrime  : chapter6.Rational = """ + $show(oneSixthPrime ));$skip(47); val res$0 = 
	//Natural equality
	oneSixthPrime == oneSixth;System.out.println("""res0: Boolean = """ + $show(res$0));$skip(56); val res$1 = 
	//vs reference equality
	oneSixthPrime equals oneSixth;System.out.println("""res1: Boolean = """ + $show(res$1));$skip(15); val res$2 = 
	
	oneHalf + 2;System.out.println("""res2: chapter6.Rational = """ + $show(res$2));$skip(47); val res$3 = 
	//But can't yet do 2 + oneThird

	oneHalf * 2;System.out.println("""res3: chapter6.Rational = """ + $show(res$3));$skip(18); val res$4 = 
	oneHalf/oneSixth;System.out.println("""res4: chapter6.Rational = """ + $show(res$4))}

}